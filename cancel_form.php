<?php

class cancel_enrolment_form extends moodleform {
    //Add elements to form
    public function definition() {
        global $CFG;
 
        $mform = $this->_form; // Don't forget the underscore! 
 
        $mform->addElement('hidden', 'userid');
        $mform->setType('userid', PARAM_INT);
        $mform->addElement('hidden', 'certifid');
        $mform->setType('certifid', PARAM_INT);
        $mform->addElement('hidden', 'returnurl');
        $mform->setType('returnurl', PARAM_URL);

        $this->add_action_buttons(true, get_string('unenrol', 'core_enrol'));
    }
    //Custom validation should be added here
    function validation($data, $files) {
        return array();
    }
}