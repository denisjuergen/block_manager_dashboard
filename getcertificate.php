<?php
require_once("../../config.php");
global $CFG, $DB, $USER, $PAGE;

require_login();

require_once($CFG->dirroot."/mod/certificate/locallib.php");
require_once($CFG->libdir."/pdflib.php");
require_once("locallib.php");

$id = required_param('id', PARAM_INT);    // Course Module ID
$userid = required_param('userid', PARAM_INT);

if(!manager_dashboard_can_view_certificate($userid)) {
    print_error('Unauthorized.');
}

if (!$cm = get_coursemodule_from_id('certificate', $id)) {
    print_error('Course Module ID was incorrect');
}
if (!$course = $DB->get_record('course', array('id'=> $cm->course))) {
    print_error('course is misconfigured');
}
if (!$certificate = $DB->get_record('certificate', array('id'=> $cm->instance))) {
    print_error('course module is incorrect');
}

$mods = get_fast_modinfo($cm->course, $userid);
$modinfo = $mods->get_cm($id);

$ci = new \core_availability\info_module($modinfo);
$info = "";
$available = $ci->is_available($info, false, $userid, $mods);
if(!$available) die("I'm sorry, this certificate isn't available for this user.");

$context = context_module::instance($cm->id); 
$PAGE->set_context($context);

$realuserid = $USER->id;
\core\session\manager::loginas($userid, $context, false);
$user = $DB->get_record('user',['id'=>$userid]);

$filename = certificate_get_certificate_filename($certificate, $cm, $course) . '.pdf';

$certrecord = certificate_get_issue($course, $user, $certificate, $cm);

require("$CFG->dirroot/mod/certificate/type/$certificate->certificatetype/certificate.php");

// PDF contents are now in $file_contents as a string.
$filecontents = $pdf->Output('', 'S');

unset($GLOBALS['USER']->realuser);
\core\session\manager::loginas($realuserid, $context, false);
unset($GLOBALS['USER']->realuser);

// Open in browser.
send_file($filecontents, $filename, 0, 0, true, false, 'application/pdf');

