<?php
$plugin->component = 'block_manager_dashboard';  // Recommended since 2.0.2 (MDL-26035). Required since 3.0 (MDL-48494)
$plugin->version = 20190060617;  // YYYYMMDDHH (year, month, day, 24-hr time)
$plugin->requires = 2015111610; // YYYYMMDDHH (This is the release version for Moodle 2.0)

$plugin->dependencies = [
    'availability_managerapproval' => 2019021001
];
