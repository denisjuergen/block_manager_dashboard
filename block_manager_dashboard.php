<?php

use core\plugininfo\availability;
require_once("{$CFG->dirroot}/blocks/moodleblock.class.php");

require_once("{$CFG->dirroot}/totara/hierarchy/prefix/organisation/lib.php");

class block_manager_dashboard extends block_base {

    public static $FIELDNAME_SEND_RECERTIFICATION_MAIL = 'sendrecertificationmail';

    public function init() {
        $this->title = get_string('dashboard', 'block_manager_dashboard');
        $this->itemsperpage = 100;
    }

     /**
     * The main content for the block.
     *
     * @return \stdClass Object containing the block content.
     */
    public function get_content() {
        global $USER, $CFG, $PAGE;

        if ($this->content !== null) {
            return $this->content;
        }

        self::check_sort_param();

        $currenttab = optional_param('mandash','manageteam', PARAM_ALPHA);
        $searchorg = optional_param('searchorganization', -1, PARAM_INT);
        $searchname = optional_param('searchname', '', PARAM_TEXT);
        $showsuspended = optional_param('showsuspended', '', PARAM_ALPHA);
        if ( $showsuspended=='on' ) {
            $showsuspended = 1;
        } else {
            $showsuspended = 0;
        }

        $my_url = $PAGE->url;
        $my_url->params( [ 'mandash' => $currenttab ]);

        require_once($CFG->libdir."/weblib.php");

        $this->content = new stdClass();
        if (empty($this->userid)) {
            // This is the default flow, the userid is typically only set for testing.
            // If it is not set we will use the current user, seeing as it is the current user we will also check that they
            // are logged in, and that they are not the guest user.
            if (!isloggedin() || isguestuser()) {
                return $this->content;
            }
            $this->userid = $USER->id;
        }

        // Create the learning data.
        $employees = self::get_employees($searchorg, $searchname, $showsuspended);
        // $employees_total = self::get_employees($searchorg, $searchname, $showsuspended, true);

        $context = context_block::instance($this->instance->id);

        $countries = get_string_manager()->get_list_of_countries();
        $mustache_countries = array();
        foreach ($countries as $key => $value) {
          $mustache_countries[] = array('key' => $key, 'value' => $value);
        }

        $languages = get_string_manager()->get_list_of_translations();
        $mustache_languages = array();
        foreach ($languages as $key => $value) {
          $mustache_languages[] = array('key' => $key, 'value' => $value);
        }

        foreach ($employees as $employee) {
          $employee->countries = array();
          foreach ($countries as $key => $value) {
            $selected = $employee->country == $key ? true : false;
            $employee->countries[] = array('key' => $key, 'value' => $value, 'selected' => $selected);
          }

          $employee->languages = array();
          foreach ($languages as $key => $value) {
            $selected = $employee->lang == $key ? true : false;
            $employee->languages[] = array('key' => $key, 'value' => $value, 'selected' => $selected);
          }
        }

        // Our block content array.
        $this->contextdata = [
            'instanceid' => $this->instance->id,
            'employees' => $employees,
            'hasemployees' => true,
            'url' => (string)$my_url,
            'ajaxurl' => (string)(new moodle_url('/blocks/manager_dashboard/ajax.php')),
            'availableorganisations' => self::get_organizations(),
            'searchorg' => $searchorg,
            'searchname' => $searchname,
            'showsuspended' => $showsuspended?'checked':'',
            'sortorder' => self::get_user_sort_order(),
            'allowcancel' => has_capability('block/manager_dashboard:allowcancel', $context),
            'can_make_manager' => has_capability('block/manager_dashboard:assignmanager', $context),
            'can_make_manager' => true,
            'countries' => $mustache_countries,
            'languages' => $mustache_languages
        ];

        $my_url->params(['sort' => 'firstname']);
        $this->contextdata['firstnamesort'] = (string)$my_url;
        $my_url->params(['sort' => 'lastname']);
        $this->contextdata['lastnamesort'] = (string)$my_url;

        // The initial view data, limited by itemsperpage.
        //$contextdata['employees'] = array_slice($contextdata['employees'], 0, $this->itemsperpage);
        if (empty($this->contextdata['employees'])) {
            $this->contextdata['hasemployees'] = false;
            $this->contextdata['no_employees'] = get_string('no_employees', 'block_manager_dashboard');
        }

        $this->content->text = '';
        // $this->content->text = self::get_tabs($currenttab);

        $core_renderer = $this->page->get_renderer('core');

        switch ($currenttab) {
            case 'manageteam':
            default:
                if (!empty($employees)) {
                    $this->contextdata['pagination'] = $this->pagination($employees);
                }

                // local_js([TOTARA_JS_DATEPICKER]);
                // build_datepicker_js("#birthdate");

                // Add availablemanagers for usage in new employee form
                $defaultorg = current($this->contextdata['availableorganisations']);

                $this->contextdata['availablemanagers'] = manager_dashboard_get_available_managers($defaultorg['id']);

                $this->contextdata['contextdata'] = json_encode($this->contextdata);
                $this->contextdata['employees'] = array_slice($this->contextdata['employees'], 0, $this->itemsperpage);
                $this->content->text .= $core_renderer->render_from_template('block_manager_dashboard/view_manage', $this->contextdata);
                break;
            // case 'approvalrequired':
            //     $this->contextdata['pending'] = self::get_approvalrequired($this->contextdata['employees']);
            //     $this->contextdata['haspending'] = count($this->contextdata['pending'])>0?true:false;
            //
            //     $this->contextdata['availablecertifications'] = self::get_available_certifications();
            //     $this->contextdata['categories'] = self::get_categories();
            //
            //     unset($this->contextdata['hasemployees']);
            //     unset($this->contextdata['employees']);
            //     // Create the pagination data if we have items to display.
            //     if (!empty($this->contextdata['pending'])) {
            //         $this->contextdata['pagination'] = $this->pagination($this->contextdata['pending']);
            //     }
            //     $this->contextdata['contextdata'] = json_encode($this->contextdata);
            //
            //     $this->contextdata['pending'] = array_slice($this->contextdata['pending'], 0, $this->itemsperpage);
            //     $this->content->text .= $core_renderer->render_from_template('block_manager_dashboard/view_approvalrequired', $this->contextdata);
            //     break;
            // case 'subscriptions':
            //     if (has_capability('block/manager_dashboard:viewsubscriptions', $context)) {
            //
            //       require_once($CFG->dirroot . '/totara/reportbuilder/lib.php');
            //
            //       $PAGE->set_url('/totara/dashboard/index.php?mandash=subscriptions');
            //
            //       global $DB;
            //
            //       $this->contextdata['table'] = '';
            //
            //       // Before any actions make sure user may actually access this report.
            //       $reportshortname = 'report_inschrijvingen_op_cursussen1';
            //       $reportrecord = $DB->get_record('report_builder', array('shortname' => $reportshortname));
            //       $globalrestrictionset = rb_global_restriction_set::create_from_page_parameters($reportrecord);
            //       $report = reportbuilder_get_embedded_report($reportshortname, null, false, 0, $globalrestrictionset);
            //       if (!$report) {
            //           print_error('error:couldnotgenerateembeddedreport', 'totara_reportbuilder');
            //       }
            //
            //       \totara_reportbuilder\event\report_viewed::create_from_report($report)->trigger();
            //
            //       $report->include_js();
            //
            //       $renderer = $PAGE->get_renderer('totara_reportbuilder');
            //
            //       list($reporthtml, $debughtml) = $renderer->report_html($report, '');
            //
            //       ob_start();
            //       $report->display_restrictions();
            //       $report->display_search();
            //       $report->display_sidebar_search();
            //       $this->contextdata['table'] .= ob_get_contents();
            //       ob_end_clean();
            //
            //       $this->contextdata['table'] .= $reporthtml;
            //
            //       $this->contextdata['contextdata'] = json_encode($this->contextdata);
            //
            //       $this->content->text .= $core_renderer->render_from_template('block_manager_dashboard/view_subscriptions', $this->contextdata);
            //
            //     }
            //
            //     break;
            // case 'certifications':
            // default:
            //     $employees = self::append_certifications($employees);
            //
            //     $this->contextdata['availablecertifications'] = self::get_available_certifications();
            //     $this->contextdata['categories'] = self::get_categories();
            //
            //     if (!empty($employees)) {
            //         $this->contextdata['pagination'] = $this->pagination($employees);
            //     }
            //     $this->contextdata['contextdata'] = json_encode($this->contextdata);
            //     $this->contextdata['employees'] = array_slice($this->contextdata['employees'], 0, $this->itemsperpage);
            //     $this->content->text .= $core_renderer->render_from_template('block_manager_dashboard/view_certifications', $this->contextdata);
        }
        return $this->content;
    }

    // private static function get_tabs($current) {
    //     $row = array();
    //     $tabs = $row = array();
    //
    //     // $row[] = new tabobject('certifications',
    //     //     new moodle_url('', array('mandash' => 'certifications')),
    //     //     get_string('certification', 'totara_certification')
    //     // );
    //     $row[] = new tabobject('manageteam',
    //         new moodle_url('', array('mandash' => 'manageteam')),
    //         get_string('teammembers', 'totara_core')
    //     );
    //     // $row[] = new tabobject('approvalrequired',
    //     //     new moodle_url('', array('mandash' => 'approvalrequired')),
    //     //     get_string('approvalrequired', 'block_manager_dashboard')
    //     // );
    //     // if (has_capability('block/manager_dashboard:viewsubscriptions', context_system::instance())) {
    //     //   $row[] = new tabobject('subscriptions',
    //     //       new moodle_url('', array('mandash' => 'subscriptions')),
    //     //       get_string('subscriptions', 'block_manager_dashboard')
    //     //   );
    //     // }
    //     $tabs[] = $row;
    //     return '';
    //     // return print_tabs($tabs, $current, null, null, true);
    // }

    public static function get_employees($organisationid = -1, $searchname = '', $showsuspended = false, $full = false)
    {
        $cachekey = "{$organisationid}-{$searchname}-{$showsuspended}";
        $cache = cache::make('block_manager_dashboard', 'employees');
        $cached = false; //$cache->get($cachekey);
        if($cached!=false) {
            if(!$searchname) return $cached;
            foreach($cached as $row => $user){
                if(strpos(strtolower($user->fullname), strtolower($searchname))===false) {
                    unset($cached[$row]);
                }
            }
            return $cached;
        }

        global $USER, $DB, $CFG;
        require_once($CFG->dirroot."/user/profile/lib.php");
        require_once($CFG->dirroot."/blocks/manager_dashboard/locallib.php");

        $orgs = self::get_organizations($organisationid);

        if($organisationid!=-1) {
            if(!\manager_dashboard_can_manage_organization($organisationid)) {
                die("Unauthorized");
            }

            $organisation = new organisation();
            $o = $organisation->get_item($organisationid);
            $orgs = [[
                'id' => $o->id,
                'fullname' => $o->fullname,
            ]];
        }

        $users = [];

        // $birthdateformat = get_string("datepickerlongyearparseformat","totara_core");
        $name_fields = get_all_user_name_fields(true);

        foreach($orgs as $o) {
            $jas = \totara_job\job_assignment::get_all_by_criteria('organisationid',$o['id']);
            $availablemanagers = manager_dashboard_get_available_managers($o['id']);

            if(count($jas)==0) continue;
            $userids = [];
            foreach ($jas as $ja) {
                $userids[] = $ja->userid;
            }

            $userids = implode(',', $userids);
            if(!$searchname) {
                $users_filtered = $DB->get_records_select('user', "suspended = :suspended AND id IN ({$userids})", [ 'suspended' => $showsuspended ],
                    '', "id,{$name_fields},username,email,suspended,country,lang,department");
            } else {
                $searchname = $DB->sql_like_escape($searchname);
                $users_filtered = $DB->get_records_select('user', "suspended = :suspended AND id IN ({$userids}) AND
                    CONCAT(firstname,' ',lastname) LIKE '%{$searchname}%'",
                    [ 'suspended' => $showsuspended ], '', "id,{$name_fields},username,email,suspended,country,lang,department");
            }

            foreach($users_filtered as $user) {
              //if ($USER->id == $user->id) continue;
              if (!$full) {
                $userjobassignment = \totara_job\job_assignment::get_first($user->id);

                if(!$user) continue;
                $user->fullname = fullname($user);
                $user->organisationname = $o["fullname"]?$o["fullname"]:'-';
                $user->organisationid = $userjobassignment->organisationid;
                $user->managerid = $userjobassignment->managerid;
                $user->availablemanagers = $availablemanagers;
                $user->ismanager = false;
                if( strpos( strtolower($userjobassignment->shortname), 'manager') !== false ) {
                    $user->ismanager = true;
                }

                //fix user suspend button status when using pagination, becaus in js 0!=false
                if($user->suspended) $user->suspended = true;
                else $user->suspended = false;

                profile_load_data($user);
                // $user->birthdate = date( $birthdateformat, $user->profile_field_birthdate );

                $custom_profile_fields = array('Imaging','Instruments','Precision','Metrology','Medical','Holdings');
                foreach ($custom_profile_fields as $field) {
                  $fieldname = 'profile_field_' . $field;
                  $user->{$fieldname} = $user->{$fieldname} == 1 ? true : false;
                }

                unset($user->password, $user->lastip, $user->picture, $user->policyagreed, $user->confirmed);
              }

              $users[$user->id] = $user;

              if (!$full) {
	              if(count($users)>=250) break 2;
              }
            }
        }

        if ($full) {
          return count($users);
        }

        $sortfunction = function ($a, $b) {
            $sortby = block_manager_dashboard::get_user_sort_order();
            if($sortby['order'] == -1) {
                return $a->{$sortby['field']} > $b->{$sortby['field']};
            } else {
                return $a->{$sortby['field']} < $b->{$sortby['field']};
            }
        };

        usort($users, $sortfunction);

        if(!$searchname) {
            $cache->set($cachekey, $users);
        }

        return $users;
    }

    public static function check_sort_param() {
        global $SESSION;
        $sort = optional_param('sort', '', PARAM_ALPHA);
        if($sort == '') return;

        $currentsort = self::get_user_sort_order();

        if($currentsort['field'] == $sort) { //inverse sort order
            if($currentsort['order']==-1) {
                $SESSION->managerdash_sortorder++;
            } else {
                $SESSION->managerdash_sortorder--;
            }
        } else {
            if($sort=='firstname') $SESSION->managerdash_sortorder = 2;
            else $SESSION->managerdash_sortorder = 0;
        }
    }

    public static function get_user_sort_order() {
        global $SESSION;
        $sort = [
            ['field' => 'lastname', 'order' => -1],
            ['field' => 'lastname', 'order' => 1],
            ['field' => 'firstname', 'order' => -1],
            ['field' => 'firstname', 'order' => 1],
        ];
        if (!isset($SESSION->managerdash_sortorder)) {
            $SESSION->managerdash_sortorder = 0;
        }
        return $sort[$SESSION->managerdash_sortorder];
    }

    public static function purge_caches() {
        $cache = cache::make('block_manager_dashboard', 'employees');
        $result = $cache->purge();
        $cache = cache::make('block_manager_dashboard', 'certifications');
        $result = $cache->purge();
    }

    // public static function get_categories() {
    //     $cache = cache::make('block_manager_dashboard', 'certifications');
    //     $cached = $cache->get('categories');
    //     if($cached!==false) return $cached;
    //     global $CFG;
    //     require_once($CFG->libdir.'/coursecatlib.php');
    //     $totalcount = 0;
    //     $certs = certif_get_certifications_page("all", "sortorder ASC",
    //         "p.id as pid,p.fullname,p.visible,p.category", $totalcount);
    //
    //     $categories = [];
    //     foreach($certs as $c) {
    //         $cat = coursecat::get($c->category, MUST_EXIST, true);
    //         if($cat->visible && !array_key_exists($cat->id,$categories)) {
    //             $catinfo = new stdClass();
    //             $catinfo->categoryname = $cat->name;
    //             $catinfo->id = $cat->id;
    //             $catinfo->parent = $cat->parent;
    //             $catinfo->certifcount = $cat->certifcount;
    //             $categories[$cat->id] = $catinfo;
    //         }
    //     }
    //     // Check foir missing parent categories
    //     foreach($categories as $c) {
    //         if($c->parent!=0 && !in_array($c->parent, $categories)) {
    //             $cat = coursecat::get($c->parent, MUST_EXIST, true);
    //             $catinfo = new stdClass();
    //             $catinfo->categoryname = $cat->name;
    //             $catinfo->id = $cat->id;
    //             $catinfo->parent = $cat->parent;
    //             $catinfo->certifcount = $cat->certifcount;
    //             $categories[$cat->id] = $catinfo;
    //         }
    //     }
    //     $cache->set('categories', array_values($categories));
    //     return array_values($categories);
    // }


    // public static function get_available_certifications() {
    //     $cache = cache::make('block_manager_dashboard', 'certifications');
    //     $cached = $cache->get('certifications');
    //     if($cached!==false) return $cached;
    //
    //     global $CFG;
    //     require_once($CFG->dirroot."/totara/certification/lib.php");
    //     require_once($CFG->libdir.'/coursecatlib.php');
    //
    //     $result = [];
    //     $totalcount = 0;
    //     $certs = certif_get_certifications_page("all", "sortorder ASC",
    //         "p.id as pid,p.fullname,p.visible,p.category",
    //         $totalcount);
    //     foreach($certs as $c) {
    //         $cat = coursecat::get($c->category, MUST_EXIST, true);
    //         if($cat->visible) {
    //             /*if(!array_key_exists($cat->id,$result)) {
    //                 $result[$cat->id] = [];
    //             }*/
    //             $result[] = (Array)$c;
    //         }
    //     }
    //
    //     $cache->set('certifications', array_values($result));
    //     return array_values($result);
    // }

    // public static function get_approvalrequired($employees) {
    //     global $DB, $CFG;
    //     $pending = [];
    //     foreach ($employees as $user) {
    //         $certifications = certif_load_all_completions($user->id);
    //         foreach ($certifications as $certification) {
    //             $status = certif_get_completion_state($certification['certcompletion']);
    //             if($status == CERTIFCOMPLETIONSTATE_WINDOWOPEN) {
    //                 $approved = true;
    //                 $courses = find_courses_for_certif($certification['certcompletion']->certifid);
    //                 foreach($courses as $course) {
    //                     $approved = (
    //                         $approved && \availability_managerapproval\condition::manager_approved($user->id, $course->id)
    //                     );
    //                 }
    //                 if($approved) continue;
    //
    //                 $pend = new stdClass();
    //                 $pend->programid = $certification['progcompletion']->programid;
    //                 $pend->certname = $DB->get_field('prog', 'fullname', ['id'=>$certification['progcompletion']->programid]);
    //                 $pend->expires_timestamp = $certification['certcompletion']->timeexpires;
    //                 $pend->expires = userdate($certification['certcompletion']->timeexpires, get_string("strftimedatefulllong",'langconfig'));
    //                 $pend->userid = $user->id;
    //                 $pend->userfullname = fullname($user);
    //                 $pending[] = $pend;
    //             }
    //         }
    //     }
    //
    //     $sortfunction = function ($a, $b) { return $a->expires_timestamp > $b->expires_timestamp; };
    //     usort($pending, $sortfunction);
    //
    //     return $pending;
    // }

    public static function get_organizations() {
        global $DB;

        $records = $DB->get_records('org');

        return array_values(array_map('get_object_vars', $records));

        //
        // $organisationid = $jobassignment->organisationid;
        //
        // /* Old:
        // /* $organisation = new organisation();
        // /* $orgs = $organisation->get_item_descendants($organisationid); */
        //
        // $path = $DB->get_field('org', 'path', array('id' => $organisationid));
        // $sql = "SELECT id, fullname FROM {org} WHERE path = ? OR " . $DB->sql_like('path', '?'). " ORDER BY fullname";
        // $orgs = $DB->get_records_sql($sql, array($path, "{$path}/%"));
        //
        // $result = [];
        // foreach($orgs as $o) {
        //     $result[] = [
        //         'id' => $o->id,
        //         'fullname' => $o->fullname,
        //     ];
        // }
        // return $result;
    }

    // public static function append_certifications($users, $dashboardtype = 'manager') {
    //     global $CERTIFSTATUS;
    //     foreach ($users as $user) {
    //         $user->currentcertifications = $user->validcertifications = $user->expiredcertifications = [];
    //         $completions = certif_load_all_completions($user->id);
    //         foreach($completions as $completion) {
    //             $status = $completion["certcompletion"]->status;
    //             switch($status) {
    //                 case CERTIFSTATUS_ASSIGNED:
    //                 case CERTIFSTATUS_INPROGRESS:
    //                     if(self::certification_approved($user, $completion)) {
    //                         $user->currentcertifications[] = self::get_certification_info($completion, false);
    //                         $user->hascurrent = true;
    //                     }
    //                 break;
    //
    //                 case CERTIFSTATUS_COMPLETED:
    //                     $user->validcertifications[] = self::get_certification_info($completion, true, fullname($user), $dashboardtype);
    //                     $user->hasvalid = true;
    //
    //                     // If approved, also add to currentcertifications
    //                     if(self::certification_approved($user, $completion)) {
    //                         $info = self::get_certification_info($completion, false);
    //                         $info->enrolled_at = self::get_current_assigment_info($completion);
    //                         $info->status = get_string('status_assigned', 'totara_certification');
    //                         $info->courselink = (string)self::get_courselink($completion["certcompletion"]->certifid);
    //                         $user->currentcertifications[] = $info;
    //                         $user->hascurrent = true;
    //                     }
    //                 break;
    //
    //                 case CERTIFSTATUS_EXPIRED:
    //                     $user->expiredcertifications[] = self::get_certification_info_from_history($completion);
    //                     $user->hasexpired = true;
    //                 break;
    //             }
    //         }
    //     }
    //     return $users;
    // }

    // private static function certification_approved($user, $completion) {
    //     $courses = find_courses_for_certif($completion["certcompletion"]->certifid, 'c.id, c.fullname', $completion["certcompletion"]->certifpath);
    //     $approved = true;
    //     foreach($courses as $dummy => $course) {
    //         if(!\availability_managerapproval\condition::manager_approved($user->id, $course->id)) {
    //             $approved = false;
    //         }
    //     }
    //     return $approved;
    // }

    // public static function get_certification_info($completion, $withcertificatelink = false, $userfullname = '', $certificatelinktype = 'manager') {
    //     global $DB, $CFG, $CERTIFSTATUS, $PAGE, $OUTPUT;
    //     require_once($CFG->dirroot."/blocks/manager_dashboard/locallib.php");
    //
    //     $result = new \stdClass();
    //     $result->status = get_string($CERTIFSTATUS[$completion["certcompletion"]->status], 'totara_certification');
    //
    //     $result->fullname = $DB->get_field('prog', 'fullname', ['id'=>$completion['progcompletion']->programid]);
    //     $result->progress = certification_progress($completion['certcompletion']->id);
    //     $result->completion_date = userdate($completion['certcompletion']->timecompleted, get_string("strftimedatefulllong",'langconfig'));
    //     $result->expire_date = $completion['certcompletion']->timeexpires==0?'':userdate(
    //         $completion['certcompletion']->timeexpires,
    //         get_string("strftimedatefulllong",'langconfig')
    //     );
    //     $result->fullname = $DB->get_field('prog', 'fullname', ['id'=>$completion['progcompletion']->programid]);
    //     $result->window_open = certif_iswindowopen($completion["certcompletion"]->certifid, $completion["certcompletion"]->userid);
    //     $result->programid = $completion['progcompletion']->programid;
    //
    //     if($withcertificatelink) $result->certificatelink = self::find_certificate_link($completion, $completion["certcompletion"]->userid, $certificatelinktype);
    //     if($userfullname) $result->userfullname = $userfullname;
    //
    //     if(in_array($completion["certcompletion"]->status, [CERTIFSTATUS_ASSIGNED, CERTIFSTATUS_INPROGRESS])) {
    //         $result->enrolled_at = self::get_current_assigment_info($completion);
    //         $courselink = self::get_courselink($completion["certcompletion"]->certifid);
    //         $result->courselink = (string)$courselink;
    //     }
    //
    //     $result->cancellink = (string)self::get_cancellink($completion["certcompletion"]);
    //     return $result;
    // }

    // private static function get_courselink($certifid) {
    //     $courses = find_courses_for_certif($certifid);
    //     $courseid = current($courses);
    //     $courseid = $courseid->id;
    //     return new moodle_url('/course/view.php',['id'=>$courseid]);
    // }

    // private static function get_cancellink($certcompletion) {
    //     global $PAGE;
    //     $returnurl = (string)$PAGE->url;
    //
    //     return new moodle_url('/blocks/manager_dashboard/cancel.php',[
    //         'certifid'=>$certcompletion->certifid,
    //         'userid'=>$certcompletion->userid,
    //         'returnurl'=>$returnurl
    //     ]);
    // }

    // private static function get_current_assigment_info($completion) {
    //     global $PAGE, $OUTPUT;
    //     $result = "";
    //     if($seminars = manager_dashboard_user_assigned_to_seminar_in_prog(
    //         $completion['certcompletion']->certifid,
    //         $completion["certcompletion"]->userid,
    //         $completion["certcompletion"]->timewindowopens
    //     )) {
    //         $data = new stdClass();
    //         $data->seminars = $seminars;
    //         if(count($seminars)==1) {
    //             $data->prefix = get_string("sessionscheduled","block_manager_dashboard");
    //         } else {
    //             $data->prefix = get_string("sessionsscheduled","block_manager_dashboard");
    //         }
    //         $renderer = $PAGE->get_renderer('core');
    //         $result = $renderer->render_from_template('block_manager_dashboard/assigned_seminars', $data);
    //     } else {
    //         if(certification_has_elearnings(
    //             $completion['progcompletion']->programid,
    //             $completion["certcompletion"]->userid
    //         )) {
    //             $result = $OUTPUT->flex_icon('mod_scorm|icon')."&nbsp;".get_string("onlinecourse","block_manager_dashboard");
    //         } else {
    //             // TODO I don't know what todo right now
    //         }
    //     }
    //     return $result;
    // }

    // private static function get_certification_info_from_history($completion, $withcertificatelink = false) {
    //     global $DB, $CERTIFSTATUS;
    //
    //     $historycompletion = $DB->get_record_sql(
    //         'SELECT * FROM {certif_completion_history}
    //         WHERE certifid = :certifid
    //         AND userid = :userid
    //         ORDER BY timeexpires DESC
    //         LIMIT 1',
    //         [
    //             'certifid' =>  $completion['certcompletion']->certifid,
    //             'userid' => $completion['certcompletion']->userid,
    //         ]);
    //
    //     if(!$historycompletion) return self::get_certification_info($completion, $withcertificatelink);
    //
    //     $result = new \stdClass();
    //     $result->status = get_string($CERTIFSTATUS[$completion["certcompletion"]->status], 'totara_certification');
    //     $result->fullname = $DB->get_field('prog', 'fullname', ['id'=>$completion['progcompletion']->programid]);
    //     $result->progress = certification_progress($completion['certcompletion']->id);
    //     $result->completion_date = userdate($historycompletion->timecompleted, get_string("strftimedatefulllong",'langconfig'));
    //     $result->expire_date = userdate($historycompletion->timeexpires, get_string("strftimedatefulllong",'langconfig'));
    //     $result->fullname = $DB->get_field('prog', 'fullname', ['id'=>$completion['progcompletion']->programid]);
    //     $result->window_open = false;
    //
    //     if($withcertificatelink) $result->certificatelink = self::find_certificate_link($completion, $completion["certcompletion"]->userid);
    //
    //     return $result;
    // }

    // private static function find_certificate_link($completion, $userid, $certificatelinktype = 'manager') {
    //     global $SIMULATE_MANAGER_APPROVED;
    //     $SIMULATE_MANAGER_APPROVED = true;
    //
    //     $courses = find_courses_for_certif($completion["certcompletion"]->certifid, 'c.id, c.fullname', $completion["certcompletion"]->certifpath);
    //     $certificate = null;
    //     foreach($courses as $course) {
    //         $mods = get_fast_modinfo($course->id, $completion["certcompletion"]->userid);
    //         $certs = $mods->get_instances_of('certificate');
    //         foreach($certs as $cert) {
    //             //check if user had access to certificate
    //             $ci = new \core_availability\info_module($cert);
    //             $info = "";
    //             $available = $ci->is_available($info, false, $userid, $mods);
    //             if(!$available) { continue; }
    //             if($certificatelinktype=='student') {
    //                 $certificate = new moodle_url('/blocks/student_dashboard/getcertificate.php',[
    //                     "id" => $cert->id
    //                 ]);
    //             } else {
    //                 $certificate = new moodle_url('/blocks/manager_dashboard/getcertificate.php',[
    //                     "userid"=>$completion["certcompletion"]->userid,
    //                     "id" => $cert->id
    //                 ]);
    //             }
    //         }
    //     }
    //
    //     unset($SIMULATE_MANAGER_APPROVED);
    //     return (string)$certificate;
    // }

    /**
     * Creates the data needed for the pagination template.
     *
     * @param stdClass[] $learning_data An array of learning data context items.
     * @return stdClass A pagination context data object.
     */
    private function pagination(array $learning_data, $full=0) {

        $data = new stdClass();

        $data->totalitems = count($learning_data);
        $data->totalitems_full = $full > 250 ? 'of total ' . $full : '';
        $data->itemsperpage = $this->itemsperpage;
        $data->currentpage = 1;
        $data->pages = null;
        $data->text = 0;
        $data->pages = array();

        if ($data->totalitems === 0) {
            return $data;
        }

        // Figure out how many pages we have.
        $pages = (int)ceil($data->totalitems / $this->itemsperpage);

        if ($pages <= 1) {
            $pages = 1;
            $data->onepage = 1;
        }

        $data->nextclass = $data->currentpage == $pages ? 'disabled' : '';
        $data->previousclass = $data->currentpage == 1 ? 'disabled' : '';


        // The display text.
        $data->text = get_string("displayingxofx", "block_current_learning", array(
            'start' => 1,
            'end' => ($data->totalitems < $data->itemsperpage) ? $data->totalitems : $data->itemsperpage,
            'total' => $data->totalitems
        ));

        $pages = range(1, $pages);

        foreach ($pages as $page) {
            $pageinfo = new \stdClass();
            $pageinfo->page = $page;
            $pageinfo->link = '';
            if ($page == $data->currentpage) {
                $pageinfo->active = 'active';
            }
            $data->pages[] = $pageinfo;
        }

        return $data;
    }

    static function get_allowed_role_mapping() {
        return [
            'student' => 'Staff',
            'manager' => 'Manager'
        ];
    }
}
