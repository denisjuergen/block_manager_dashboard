<?php

define('AJAX_SCRIPT', true);

require_once('../../config.php');
require_once('locallib.php');
require_once($CFG->dirroot.'/user/lib.php');

global $DB;

require_login();
$PAGE->set_url('/blocks/manager_dashboard/ajax.php');
$PAGE->set_context(context_system::instance());

$userid = required_param('userid',PARAM_INT);
$action = required_param('action',PARAM_ALPHA);

$birthdateformat = get_string("datepickerlongyearparseformat","totara_core");

$result = new stdClass();
$result->result = 'FAILED';
$result->id = $userid;

$canview = manager_dashboard_can_view_certificate($userid);

if($action=='unsuspend') {
    $canview = manager_dashboard_can_view_certificate($userid, true);
}

if($action=='adduser' || $action=='getmanagers' || $action=='changeponame' || $action=='exportlist') {
    $canview = $userid==-1;
}

$custom_profile_fields = array('Imaging','Instruments','Precision','Metrology','Medical','Holdings');

if($canview) {
    switch ($action) {
        // case 'changeponame':
        //   $rowid = required_param('rowid',PARAM_INT);
        //   $newtitle = required_param('newtitle',PARAM_TEXT);
        //   $response = false;
        //
        //   $log = $DB->get_record('logstore_standard_log', array('id' => $rowid), '*', MUST_EXIST);
        //
        //   $other = @unserialize($log->other);
        //   if (!$other) {
        //     $result->error = 'Updating PO-number has failed';
        //   } else {
        //
        //     $other['orderrefrence'] = $newtitle;
        //     $log->other = serialize($other);
        //
        //     $response = $DB->update_record('logstore_standard_log', $log);
        //   }
        //
        //   if (!$response) {
        //     $result->error = 'Updating PO-number has failed';
        //   } else {
        //     $result->message = 'gelukt!';
        //     $result->result = 'OK';
        //     block_manager_dashboard::purge_caches();
        //   }
        // break;

        case 'suspend':
            $DB->set_field('user', 'suspended', 1, ['id'=>$userid]);
            \core\session\manager::kill_user_sessions($userid);

            \block_manager_dashboard\event\user_suspended::create(['relateduserid' => $userid])->trigger();

            $result->message = get_string('suspended');
            $result->result = 'OK';
            block_manager_dashboard::purge_caches();
        break;

        case 'unsuspend':
            $DB->set_field('user', 'suspended', 0, ['id'=>$userid]);

            \block_manager_dashboard\event\user_unsuspended::create(array('relateduserid' => $userid))->trigger();

            $result->message = get_string('unsuspended', "block_manager_dashboard");
            $result->result = 'OK';
            block_manager_dashboard::purge_caches();
        break;

        case 'exportlist':
            $employees = block_manager_dashboard::get_employees();
            $data = array();
            foreach ($employees as $user) {

              if ($user->profile_field_Contracttype == '0' ) {
                $user->profile_field_Contracttype = 'Regular employee';
              } elseif ($user->profile_field_Contracttype == '1') {
                $user->profile_field_Contracttype = 'Contracted or temporary employee';
              }

              $manager = '';

              if (!empty($user->managerid)) {
                $manageruser = $DB->get_record('user', ['id' => $user->managerid]);
                if (!empty($manageruser)) {
                  $manager = fullname($manageruser);
                }
              }

              $user->ismanager = $user->ismanager == true ? '1' : '0';

              $data[$user->id] = array('id' => $user->id,
                                       'firstname' => $user->firstname,
                                       'lastname' => $user->lastname,
                                       'username' => $user->username,
                                       'email' => $user->email,
                                       'department' => $user->department,
                                       'country' => $user->country,
                                       'language' => $user->lang,
                                       'fullname' => $user->fullname,
                                       'manager' => $manager,
                                       'is_manager' => $user->ismanager,
                                       'organisationname' => $user->organisationname,
                                       'profile_field_Contracttype' => $user->profile_field_Contracttype,
                                     );

                foreach ($custom_profile_fields as $field) {
                  $fieldname = 'profile_field_' . $field;
                  $data[$user->id][$fieldname] = ($user->{$fieldname} == true) ? '1' : '0';
                }

            }

            $filename = "userexport";
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename={$filename}.csv");
            header("Pragma: no-cache");
            header("Expires: 0");
            $headers = array_keys(reset($data));
            $data = array_merge([$headers],$data);
            $outputBuffer = fopen("php://output", 'w');
            foreach($data as $val) {
                fputcsv($outputBuffer, $val, ';');
            }
            fclose($outputBuffer);
            die;
        break;

        case 'resetuserpassword':
            if(manager_dashboard_reset_password($userid)) {
                \block_manager_dashboard\event\user_password_reset::create(array('relateduserid' => $userid))->trigger();

                $result->message = get_string('passwordreset:sent', 'block_manager_dashboard');
                $result->result = 'OK';
            } else {
                $result->message = get_string('passwordreset:failed', 'block_manager_dashboard');
            }
        break;

        case 'adduser':
            // $username = required_param('username',PARAM_USERNAME);
            $firstname = required_param('firstname',PARAM_RAW);
            $lastname = required_param('lastname',PARAM_RAW);
            $username = substr(strtolower($firstname), 0, 1) . str_replace(" ", "", strtolower($lastname));
            if ($DB->record_exists('user', array('username' => $username))) {
                $username .= '1';
                if ($DB->record_exists('user', array('username' => $username))) {
                    $username .= '1';
                }
            }
            // $birthdate = required_param('birthdate',PARAM_TEXT);
            $email = strtolower(required_param('email', PARAM_EMAIL));
            $department = required_param('department', PARAM_TEXT);
            $organization = required_param('organization', PARAM_INT);
            $manager = required_param('manager', PARAM_INT);
            $country = required_param('country', PARAM_TEXT);
            $lang = required_param('language', PARAM_TEXT);
            $contracttype = optional_param('contracttype', null, PARAM_RAW);

            if (manager_dashboard_can_manage_organization($organization)) {
                if ($manager) {
                    $validmanager = manager_dasboard_is_valid_manager_for_org($organization, $manager);
                    if (!$validmanager) {
                        $result->errors['manager'] = get_string('invalid_manager_selected',"block_manager_dashboard");
                        break;
                    }
                }

                $authplugin = get_auth_plugin('manual');

                $user = new stdClass();
                $user->timemodified = time();
                $user->timecreated = time();
                $user->firstname = $firstname;
                $user->lastname = $lastname;
                $user->email = $email;
                $user->department = $department;
                $user->lang = $lang;
                $user->username = $username;
                $user->idnumber = $username;
                $user->auth = 'manual';
                $user->mnethostid = $CFG->mnet_localhost_id;
                $user->confirmed = 1;
                $user->country = $country;

                $result->errors = [];

                // if (empty($birthdate)) {
                //     $result->errors['birthdate'] = get_string('required');
                // } else {
                //     $dt = \DateTime::createFromFormat($birthdateformat, $birthdate);
                //     if($dt === false || array_sum($dt->getLastErrors())) {
                //         $result->errors['birthdate'] = get_string('required');
                //     } else {
                //         $user->profile_field_birthdate = $dt->getTimestamp();
                //     }
                // }

                $missing_required = true;
                foreach ($custom_profile_fields as $field) {
                    $field_value = optional_param($field, null, PARAM_INT);

                    if ( !empty($field_value) ) {
                        $name = 'profile_field_' . $field;
                        $user->{$name} = $field_value;
                        $missing_required = false;
                    }
                }

                if (empty($user->firstname)) {
                    $result->errors['firstname'] = get_string('required');
                }

                if (empty($user->lastname)) {
                    // Might be only whitespace.
                    $result->errors['lastname'] = get_string('required');
                }

                if (empty($user->country)) {
                    $result->errors['country'] = get_string('required');
                }

                if (empty($user->lang)) {
                    $result->errors['language'] = get_string('required');
                }

                if (empty($manager)) {
                    $result->errors['manager'] = get_string('required');
                }

                if ( $contracttype === '' ) {
                  $result->errors['contracttype'] = get_string('required');
                }
                elseif ( $contracttype !== null ) {
                    $user->profile_field_Contracttype = $contracttype;
                }

                if ($missing_required) {
                  foreach ($custom_profile_fields as $field) {
                    $result->errors[$field] = get_string('required');
                  }
                }

                if (empty($user->username)) {
                    // Might be only whitespace.
                    // $result->errors['username'] = get_string('required');
                } else {

                    if ($DB->record_exists('user', array('username' => $user->username))) {
                        $user->username .= '1';
                        $user->idnumber .= '1';
                        if ($DB->record_exists('user', array('username' => $user->username))) {
                            $user->username .= date('d');
                            $user->idnumber .= date('d');
                        }
                    }

                    // Check allowed characters.
                    // if ($user->username !== \core_text::strtolower($user->username)) {
                    //     $result->errors['username'] = get_string('usernamelowercase');
                    //  }
                    // else {
                    //     if ($user->username !== \core_user::clean_field($user->username, 'username')) {
                    //         $result->errors['username'] = get_string('invalidusername');
                    //     }
                    // }
                }

                if (!validate_email($user->email)) {
                    $result->errors['email'] = get_string('invalidemail');

                } elseif ($DB->record_exists('user', array('email' => $user->email))) {
                    $result->errors['email'] = get_string('emailexists');
                }

                if (count($result->errors)==0) {
                    require_once($CFG->dirroot.'/user/profile/lib.php');

                    $newuserid = user_create_user($user,true,true);
                    $user->id = $newuserid;
                    set_user_preference('auth_forcepasswordchange', 1, $user->id);
                    set_user_preference('create_password',          1, $user->id);
                    profile_save_data($user);

                    $result->fullname = fullname($user);
                    $result->email = $user->email;

                    \totara_job\job_assignment::create_default($user->id);
                    $org = manager_dashboard_update_organisation($user, $organization, $manager);
                    $result->organization = $org->fullname;

                    \block_manager_dashboard\event\user_added::create(array('relateduserid' => $newuserid))->trigger();

                    $result->message = get_string('updated', 'core', fullname($user));
                    $result->result = 'OK';
                    block_manager_dashboard::purge_caches();
                }
            }
        break;

        case 'updateuser':
            // $username = required_param('username', PARAM_USERNAME);
            $firstname = required_param('firstname', PARAM_RAW);
            $lastname = required_param('lastname', PARAM_RAW);
            $username = substr(strtolower($firstname), 0, 1) . str_replace(" ", "", strtolower($lastname));
            // $birthdate = required_param('birthdate', PARAM_TEXT);
            $email = strtolower(required_param('email', PARAM_EMAIL));
            $organization = required_param('organization', PARAM_INT);
            $managerid = required_param('managerid', PARAM_INT);
            $country = required_param('country', PARAM_TEXT);
            $department = required_param('department', PARAM_TEXT);
            $contracttype = optional_param('contracttype', null, PARAM_INT);
            $lang = optional_param('language', null, PARAM_TEXT);

            if(manager_dashboard_can_manage_organization($organization)) {
                if($manager) {
                    $validmanager = manager_dasboard_is_valid_manager_for_org($organization, $managerid);
                    if(!$validmanager) {
                        $result->errors['manager'] = get_string('invalid_manager_selected',"block_manager_dashboard");
                        break;
                    }
                }

                $authplugin = get_auth_plugin('manual');

                $user = $DB->get_record('user', ['id' => $userid]);
                $usernew = clone $user;
                $usernew->timemodified = time();
                $usernew->firstname = $firstname;
                $usernew->lastname = $lastname;
                $usernew->email = $email;
                $usernew->department = $department;
                $usernew->username = $username;
                $usernew->idnumber = $username;
                $usernew->country = $country;
                $usernew->lang = $lang;

                $result->errors = [];

                // if (empty($birthdate)) {
                //     $result->errors['birthdate'] = get_string('required');
                // } else {
                //     $dt = \DateTime::createFromFormat($birthdateformat, $birthdate);
                //     if($dt === false || array_sum($dt->getLastErrors())) {
                //         $result->errors['birthdate'] = get_string('required');
                //         $result->info = "$birthdateformat, $birthdate";
                //     } else {
                //         $usernew->profile_field_birthdate = $dt->getTimestamp();
                //     }
                // }

                if (empty($usernew->username)) {
                    // Might be only whitespace.
                    // $result->errors['username'] = get_string('required');
                } else if ($user->username !== $usernew->username) {

                    if ($DB->record_exists('user', array('username' => $usernew->username))) {
                        $user->username .= '1';
                    }
                    // // Check new username does not exist.
                    // if ($DB->record_exists('user', array('username' => $usernew->username))) {
                    //     $result->errors['username'] = get_string('usernameexists');
                    // }
                    // // Check allowed characters.
                    // if ($usernew->username !== core_text::strtolower($usernew->username)) {
                    //     $result->errors['username'] = get_string('usernamelowercase');
                    // }
                    // else {
                    //     if ($usernew->username !== core_user::clean_field($usernew->username, 'username')) {
                    //         $result->errors['username'] = get_string('invalidusername');
                    //     }
                    // }
                }

                if ( $contracttype !== false ) {
                    $usernew->profile_field_Contracttype = $contracttype;
                }

                if (!$user or (isset($usernew->email) && strtolower($user->email) !== $usernew->email)) {
                    if (!validate_email($usernew->email)) {
                        $result->errors['email'] = get_string('invalidemail');
                    } else if (empty($CFG->allowaccountssameemail)
                            and $DB->record_exists('user', array('email' => $usernew->email))) {
                                $result->errors['email'] = get_string('emailexists');
                    }
                }

                if (count($result->errors) == 0) {
                    require_once($CFG->dirroot.'/user/profile/lib.php');

                    if (!$authplugin->user_update($user, $usernew)) {
                        $result->errors[] = get_string('cannotupdateprofile');
                    } else {

                        $custom_profile_fields = array('Imaging','Instruments','Precision','Metrology','Medical','Holdings');
                        foreach ($custom_profile_fields as $field) {
                            $field_value = optional_param($field . $userid, null, PARAM_INT);
                            $name = 'profile_field_' . $field;
                            $usernew->{$name} = $field_value;
                        }

                        user_update_user($usernew, false, true);
                        profile_save_data($usernew);

                        \block_manager_dashboard\event\user_updated::create(array('relateduserid' => $userid))->trigger();

                        $result->fullname = fullname($usernew);
                        $result->username = $user->username;
                        $result->email = $usernew->email;

                        $org = manager_dashboard_update_organisation($usernew, $organization, $managerid);
                        $result->organization = $org->fullname;

                        $result->message = get_string('updated', 'core', fullname($usernew));
                        $result->result = 'OK';
                    }
                    block_manager_dashboard::purge_caches();
                }
            }
        break;

        // case 'assign':
        //     /*
        //     *  Assign a user to a program.
        //     *  Whether a user should attend a face to face meeting.
        //     *  pid = certificationid = program id.
        //     *  session = f2f meeting.
        //     */
        //     require_once($CFG->dirroot.'/totara/program/program.class.php');
        //     $pid = required_param('certificationid', PARAM_INT);
        //     if (!manager_dashboard_can_assign_certificate($pid)) {
        //         $result->message = "Unauthorized!";
        //         break;
        //     }
        //
        //     manager_dashboard_clear_old_grades($pid, $userid);
        //
        //     // get the optional session id.
        //     $sessionid = optional_param('session', -1, PARAM_INT);
        //     // find all f2f seminars for the prog/cert.
        //     $sessions = find_seminar_dates_for_certification($pid, $userid);
        //
        //     $enrolledsession = -1;
        //
        //     // When a session is requested, check if there's a valid session.
        //     // If so, subscribe the user.
        //     if (is_array($sessions) && $sessionid != -1) {
        //
        //         $validsession = array_filter($sessions, function($session) use ($sessionid) {
        //             return $session->id == $sessionid;
        //         });
        //
        //         if (!$validsession) {
        //             $result->message = "2 Unauthorized!";
        //             break;
        //         }
        //
        //         // assign the user to the f2f session
        //         $sessionrecord = $DB->get_record('facetoface_sessions', ['id'=>$sessionid]);
        //         $facetoface = $DB->get_record('facetoface', ['id'=>$sessionrecord->facetoface]);
        //         $course = $DB->get_record('course', ['id'=>$facetoface->course]);
        //         //signup user
        //         $result->signup = facetoface_user_signup(
        //             $sessionrecord,
        //             $facetoface,
        //             $course,
        //             '',
        //             MDL_F2F_INVITE,
        //             MDL_F2F_STATUS_BOOKED,
        //             $userid
        //         );
        //         // If approval required, just approve
        //         if(facetoface_approval_required($facetoface)) {
        //             $result->approving = 'yes';
        //             $data = new stdClass();
        //             $data->s = $sessionid;
        //             $data->requests = [ $userid => 2 ];
        //             $result->approveresult = facetoface_approve_requests($data);
        //         }
        //         $enrolledsession = $sessionid;
        //     }
        //
        //     // When no session is requested, add the user to the (online) program.
        //     // The cron will make sure the program becomes visible in the dashboard.
        //     $program = new program($pid);
        //     manager_dashboard_assign_to_program($pid, $userid, true);
        //
        //     $orderrefrence = required_param("orderrefrence", PARAM_RAW);
        //
        //     $user = $DB->get_record('user', ['id' => $userid]);
        //     $info = new stdClass();
        //     $info->name = fullname($user);
        //     $info->certification = $program->fullname;
        //
        //     manager_dashboard_manager_approval_courses($pid, $userid);
        //
        //     global $PAGE;
        //     $completion = certif_load_completion($program->id, $userid);
        //     // Fix for completion
        //     $completion['certcompletion'] = $completion[0];
        //     $completion['progcompletion'] = $completion[1];
        //
        //     $contextdata = block_manager_dashboard::get_certification_info($completion, false);
        //
        //     $core_renderer = $PAGE->get_renderer('core');
        //     $result->newrow = $core_renderer->render_from_template('block_manager_dashboard/certification_current_row', $contextdata);
        //
        //     $sessionname = '';
        //     $sessions = find_seminar_dates_for_certification($program->id, $userid, $sessionname);
        //     if (!$sessionname) $sessionname = certification_has_elearnings($program->id, $userid);
        //
        //     $other = array('orderrefrence' => $orderrefrence, 'activityname' => $sessionname);
        //     if($enrolledsession && $enrolledsession!=-1) { $other['sessionid'] = $enrolledsession; }
        //     \block_manager_dashboard\event\user_assigned::create(array('relateduserid' => $userid, 'objectid' => $pid, 'other' => $other))->trigger();
        //
        //     $result->programid = $pid;
        //     $result->result = 'OK';
        //     $result->message = get_string("assigned", "block_manager_dashboard", $info);
        //     block_manager_dashboard::purge_caches();
        // break;

        case 'getcertificationdates':
            require_once($CFG->dirroot.'/totara/program/program.class.php');

            $pid = required_param('certificationid', PARAM_INT);
            if (!manager_dashboard_can_assign_certificate($pid)) {
                $result->message = "Unauthorized! 0x01";
                break;
            }
            $program = new program($pid);

            $expiretime = -1;
            // If assigned, show only dates before expiration
            $assignments = $DB->get_records('prog_user_assignment', ['userid'=>$userid, 'programid'=>$program->id]);
            if(count($assignments)) {
                $completion = certif_load_completion($program->id, $userid);
                $state = certif_get_completion_state($completion[0]);
                $expiretime = $completion[0]->timeexpires;
                if($state==CERTIFCOMPLETIONSTATE_EXPIRED) {
                    $expiretime = -1;
                }
            }

            $sessionname = "";
            $sessions = find_seminar_dates_for_certification($program->id, $userid, $sessionname, $expiretime);
            if (is_array($sessions)) {
          		if (empty($sessions)) {
          			$result->contact = true;
          		}
                $result->sessions = $sessions;
                $result->sessionname = $sessionname;
                $result->iselearning = false;
            } else if ($result->sessionname = certification_has_elearnings($program->id, $userid)) {
                $result->iselearning = true;
            } else {
                $result->contact = true;
            }
            $result->result = 'OK';
        break;

        case 'makemanager':
            $newrole = required_param('newrole', PARAM_ALPHA);
            $roles = block_manager_dashboard::get_allowed_role_mapping();
            if(!array_key_exists($newrole, $roles)) {
                $result->error = 'Unauthorized 0x01';
                break;
            }

            if(!manager_dashboard_update_role($userid, $newrole)) {
                 $result->error = 'Unauthorized 0x02';
                break;
            }
            if($newrole=='manager') {
                $result->newtext = get_string('make_student','block_manager_dashboard');
                $result->newrole = 'student';
            } else {
                $result->newtext = get_string('make_manager','block_manager_dashboard');
                $result->newrole = 'manager';
            }
            $result->message = get_string('updated', 'core', 'user');
            $result->result = 'OK';
        break;

        case 'getmanagers':
            $org = required_param('orgid', PARAM_INT);
            $result->managers = manager_dashboard_get_available_managers($org);
            $result->result = 'OK';
        break;

    }

} else {
    $result->error = 'Unauthorized 0xFF';
}

echo $OUTPUT->header();
echo json_encode($result);

die();
