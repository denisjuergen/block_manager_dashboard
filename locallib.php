<?php
defined('MOODLE_INTERNAL') || die();

function manager_dashboard_can_view_certificate($userid, $suspended = false) {
    global $CFG, $DB;
    require_once("block_manager_dashboard.php");
    require_once("{$CFG->dirroot}/totara/hierarchy/prefix/organisation/lib.php");

    $where = [
        'deleted' => 0,
        'suspended' => $suspended?1:0,
        'id' => $userid
    ];
    $employee = $DB->get_record('user', $where );
    if(!$employee) return false;

    $jobassignment = \totara_job\job_assignment::get_first($employee->id, false);
    if(!$jobassignment) return false;

    if(manager_dashboard_can_manage_organization($jobassignment->organisationid)) {
        return true;
    }
    return false;
}

function manager_dashboard_user_assigned_to_seminar_in_prog($progid, $userid, $from = 0) {
    global $DB, $CFG;
    require_once($CFG->dirroot."/mod/facetoface/lib.php");
    $sessions = [];
    $courses = find_courses_for_certif($progid);
    $courses = array_keys($courses);
        $records = $DB->get_records_sql("SELECT sessions.id, signups.* FROM {facetoface} as facetoface
        LEFT JOIN {facetoface_sessions} as sessions ON sessions.facetoface = facetoface.id
        LEFT JOIN {facetoface_signups} as signups ON signups.sessionid = sessions.id
        JOIN {facetoface_signups_status} as ss ON signups.id = ss.signupid
        WHERE signups.userid = :userid
        AND facetoface.course IN (:courseids)
        AND ss.statuscode NOT IN (:excludestates)
        AND ss.superceded = 0",
    ['userid'=>$userid, 'courseids' => implode(',',$courses), 'excludestates'=>MDL_F2F_STATUS_USER_CANCELLED.','.MDL_F2F_STATUS_SESSION_CANCELLED]);
    foreach($records as $r) {
        $sessiondates = facetoface_get_session_dates($r->sessionid);
        foreach($sessiondates as $session ) {
            if($session->timestart > $from) {
                $session->formatteddates = manager_dashboard_format_session_dates($session);
                $sessions[] = $session;
            }
        }
    }

    if(count($sessions)) return $sessions;
    return false;
}

function manager_dashboard_format_session_dates($session) {
    $result = "";
    $a = new stdClass();
    $a->userdate_start = userdate($session->timestart, get_string("strftimedatefulllong",'langconfig'));
    $a->userdate_finish = userdate($session->timefinish, get_string("strftimedatefulllong",'langconfig'));
    $a->starttime = userdate($session->timestart, get_string("strftimeshort",'langconfig'));
    $a->finishtime = userdate($session->timefinish, get_string("strftimeshort",'langconfig'));

    if($a->userdate_start == $a->userdate_finish) {
        $result = get_string("singledaysession",'block_manager_dashboard', $a);
    } else {
        $result = get_string("multidaysession",'block_manager_dashboard', $a);
    }
    return $result;
}

function manager_dashboard_can_assign_certificate($progid) {
    global $CFG, $USER;
    require_once("block_manager_dashboard.php");

    $certifications = block_manager_dashboard::get_available_certifications();
    foreach($certifications as  $c) {
        if ($c['pid'] == $progid) return true;
    }
    return false;
}

function manager_dashboard_can_manage_organization($orgid) {
    global $CFG, $USER;

    require_once("block_manager_dashboard.php");
    require_once("{$CFG->dirroot}/totara/hierarchy/prefix/organisation/lib.php");

    $orgs =  block_manager_dashboard::get_organizations();
    foreach($orgs as $o) {
        if($o['id'] == $orgid) {
            return true;
        }
    }
    return false;
}

function manager_dasboard_is_valid_manager_for_org($orgid, $managerid) {
    $availablemanagers = manager_dashboard_get_available_managers($orgid);
    foreach($availablemanagers as $am) {
        if($am["manager_id"]==$managerid) {
            return true;
        }
    }
    return false;
}

function manager_dashboard_get_available_managers($orgid) {
    global $DB;
    $managers = [];

    // copied from the function \totara_job\job_assignment::get_all_by_criteria() and modified:
    $sql = "SELECT * FROM {job_assignment} WHERE organisationid = :organisationid AND shortname LIKE :shortname";
    $params = [
        'organisationid' => $orgid,
        'shortname' => '%Manager%'
    ];

    $managerposses = $DB->get_records_sql($sql, $params);
    foreach ($managerposses as $managerpos) {
        $manageruser = $DB->get_record('user', ['id' => $managerpos->userid, 'deleted' => 0, 'suspended' => 0]);
        if($manageruser) {
            $a = [
                'manager_id' => $manageruser->id,
                'manager_fullname' => fullname($manageruser)
            ];
            $managers[] = $a;
        }
    }
    return $managers;
}

function manager_dashboard_reset_password($userid) {
    global $DB, $CFG;

    require_once($CFG->dirroot.'/login/lib.php');

    $user = $DB->get_record('user', ['id'=>$userid]);
    if(!$user) return false;

    // The account the requesting user claims to be is entitled to change their password.
    // Next, check if they have an existing password reset in progress.
    $resetinprogress = $DB->get_record('user_password_resets', array('userid' => $user->id));
    if (!empty($resetinprogress)) {
        $DB->delete_records('user_password_resets', array('id' => $resetinprogress->id));
    }
    $resetrecord = core_login_generate_password_reset($user);
    return send_password_change_confirmation_email($user, $resetrecord);
}

function manager_dashboard_update_organisation($user, $neworgid, $managerid = false) {
    global $USER, $DB;
    $userjobassignment = \totara_job\job_assignment::get_first($user->id);

    $organisation = new organisation();
    if( $userjobassignment->organisationid == $neworgid && !$managerid) {
        return $organisation->get_item($userjobassignment->organisationid);
    }

    $roles = block_manager_dashboard::get_allowed_role_mapping();

    $update = [
        'organisationid' => $neworgid,
        'shortname' => $roles['student'],
        'fullname' => $roles['student'],
        // 'totarasync' => true
    ];
    if($managerid) {
        $managerjobassignment = \totara_job\job_assignment::get_first($managerid);
        $update['managerjaid'] = $managerjobassignment->id;
    }

    $userjobassignment->update($update);

    $org = $organisation->get_item($userjobassignment->organisationid);
    $DB->set_field('user', 'institution', $org->fullname, ['id'=>$user->id]);

    block_manager_dashboard::purge_caches();

    return $org;
}

function manager_dashboard_update_role($userid, $newrole) {
    global $USER, $DB;
    $rolemapping = block_manager_dashboard::get_allowed_role_mapping();
    if(!array_key_exists($newrole, $rolemapping)) return false;

    $userjobassignment = \totara_job\job_assignment::get_first($userid);
    $update = [
        'shortname' => $rolemapping[$newrole],
        'fullname' => $rolemapping[$newrole]
    ];
    $userjobassignment->update($update);
    block_manager_dashboard::purge_caches();
    return true;
}

 /**
     * Assign users to a program.
     *
     * @param int $programid Program id
     * @param int $userid user to be assigned to the program.
     * @param bool $updatelearnerassignments - true to run update program user assignments immediately afterwards
     *       Added in Totara 2.9.19, 9.7, 10
     * @return void since Totara 10 (previously returned bool for whether not exceptions are generated).
     */
function manager_dashboard_assign_to_program($programid, $userid, $updatelearnerassignments = true) {
    global $CFG, $DB;
    $program = new program($programid);
    $programdata = customfield_get_data($program, 'prog', 'program', false);

    // First check if user is already assigned
    $assignments = $DB->get_records('prog_user_assignment', ['userid'=>$userid, 'programid'=>$programid]);
    if(count($assignments)) {
        // Send recertification notification
        if(
            array_key_exists(\block_manager_dashboard::$FIELDNAME_SEND_RECERTIFICATION_MAIL,$programdata) &&
            strtolower($programdata[\block_manager_dashboard::$FIELDNAME_SEND_RECERTIFICATION_MAIL]) == "yes"
        ) {
            manager_dashboard_send_assign_notification($programid, $userid);
        }
        return;
    }

    require_once($CFG->dirroot . '/totara/program/lib.php');

    // Create data.
    $data = new stdClass();
    $data->id = $programid;
    $data->item = array(ASSIGNTYPE_INDIVIDUAL => array($userid => 1));
    $data->completiontime = array(ASSIGNTYPE_INDIVIDUAL => array($userid => COMPLETION_TIME_NOT_SET));
    $data->completionevent = array(ASSIGNTYPE_INDIVIDUAL => array($userid => COMPLETION_EVENT_NONE));
    $data->completioninstance = array(ASSIGNTYPE_INDIVIDUAL => array($userid => 0));
    $data->includechildren = array (ASSIGNTYPE_INDIVIDUAL => array($userid => null));

    // Assign item to program.
    $assignmenttoprog = prog_assignments::factory(ASSIGNTYPE_INDIVIDUAL);
    $assignmenttoprog->update_assignments($data, false);

    if ($updatelearnerassignments) {
        $program->update_learner_assignments(true);

        // Get the courses in this program or certification.
        global $DB;
        $certifid = $DB->get_field('prog','certifid',['id' => $programid]);
        $courses = find_courses_for_certif($certifid);

        if (!empty($courses)) {
            $user = new stdClass();
            $user->id = $userid;
            foreach ($courses as $id => $course) {
                prog_can_enter_course($user, $course);
            }
        }
    }

    block_manager_dashboard::purge_caches();
}

function certification_has_elearnings($programid, $userid) {
    global $DB, $SIMULATE_MANAGER_APPROVED;
    $SIMULATE_MANAGER_APPROVED = true;
    $certifid = $DB->get_field('prog','certifid',['id' => $programid]);
    $courses = find_courses_for_certif($certifid);
    foreach($courses as $course) {
        $mods = get_fast_modinfo($course->id);
        $scorms = $mods->get_instances_of('scorm');
        foreach($scorms as $scorm) {
            $ci = new \core_availability\info_module($scorm);
            $is_available = $ci->is_available($scorm, false, $userid);
            //$is_available = $ci->is_user_visible($scorm, $userid, false);
            if($is_available) {
                unset($SIMULATE_MANAGER_APPROVED);
                return $ci->get_course_module()->name;
            }
        }
    }
    unset($SIMULATE_MANAGER_APPROVED);
    return false;
}


function manager_dashboard_manager_approval_courses($programid, $userid) {
    global $DB;
    $certifid = $DB->get_field('prog','certifid',['id' => $programid]);
    $courses = find_courses_for_certif($certifid);
    foreach($courses as $course) {
        \availability_managerapproval\condition::approve($userid, $course->id);
    }
}

function find_seminar_dates_for_certification($programid, $userid, &$sessionname = "" , $until = -1) {
    global $DB, $SIMULATE_MANAGER_APPROVED;
    $SIMULATE_MANAGER_APPROVED = true;
    $dates = [];
    // Don't know why, but we need a certifid for this
    $certifid = $DB->get_field('prog','certifid',['id' => $programid]);
    $courses = find_courses_for_certif($certifid);
    $hasseminar = false;
    foreach($courses as $course) {
        $mods = get_fast_modinfo($course->id);
        $f2fs = $mods->get_instances_of('facetoface');
        foreach($f2fs as $f2f) {
            $ci = new \core_availability\info_module($f2f);
            $s = "";
            $is_available = $ci->is_available($s, false, $userid);
            if(!$is_available) { continue; }

            $sessions = facetoface_get_sessions($f2f->instance);
            foreach($sessions as $session) {
                $hasseminar = true;
                $past = false;
                $has_capacity = facetoface_session_has_capacity($session);
                $newsession = new stdClass();
                $newsession->id = $session->id;
                $newsession->isfull = !$has_capacity;
                $newsession->dates = [];
                foreach ($session->sessiondates as $sessiondate) {
                    $room = facetoface_get_room($sessiondate->roomid);
                    $roomstring = '';
                    if (!empty($room)) {
                        $roomstring = $room->{"customfield_building"}; //facetoface_room_to_string($room);
                    }
                    $s = new stdClass();
                    $s->room = $roomstring;
                    if($sessiondate->timestart < time()) $past = true;
                    $s->start = userdate($sessiondate->timestart);
                    $s->end = userdate($sessiondate->timefinish);
                    $s->starttimestamp = $sessiondate->timestart;
                    $newsession->dates[] = $s;
                }
                $signupcount = facetoface_get_num_attendees($session->id);

                $newsession->signupcount = $signupcount;
                $newsession->capacity = $session->capacity;

                $newsession->free = $session->capacity - $signupcount;
                $newsession->freetext = get_string('free', 'block_manager_dashboard', $newsession->free);
                if($newsession->free == 1) {
                    $newsession->freetext = get_string('single', 'block_manager_dashboard');
                } else if ($newsession->free < 1) {
                    $newsession->freetext = get_string('full', 'block_manager_dashboard');
                }

                if(!$past) $dates[$session->id] = $newsession;

                $sessionname = $f2f->name;
            }
            break;
        }
    }
    unset($SIMULATE_MANAGER_APPROVED);
    if(!$hasseminar) return -1;

    //sort seminars by startdate
    $sortfunction = function($a, $b) {
        //find oldest sessions for both
        $olda = -1;
        foreach($a->dates as $d) {
            if($olda==-1||$d->starttimestamp<$olda) $olda = $d->starttimestamp;
        }
        $oldb = -1;
        foreach($b->dates as $d) {
            if($oldb==-1||$d->starttimestamp<$oldb) $oldb = $d->starttimestamp;
        }
        if ($olda == $oldb) {
            return 0;
        }
        return ($olda < $oldb) ? -1 : 1;
    };
    usort($dates, $sortfunction);

    // if $until is set, filter out session after $until
    if($until!=-1) {
        foreach($dates as $key => $session) {
            foreach ($session->dates as $date) {
                if($date->starttimestamp > $until) {
                    unset($dates[$key]);
                    break;
                }
            }
        }
    }

    return $dates;
}

function manager_dashboard_send_assign_notification($programid, $userid) {
    global $DB, $CFG;
    require_once("{$CFG->dirroot}/totara/hierarchy/prefix/organisation/lib.php");

    $program = $DB->get_record('prog',['id'=>$programid]);
    $user = $DB->get_record('user',['id'=>$userid]);

    $jobassignment = \totara_job\job_assignment::get_first($userid);
    $organisation = new \organisation();
    $org = $organisation->get_item($jobassignment->organisationid);

    $a = new stdClass();
    $a->certification = $program->fullname;
    $a->userfullname = fullname($user);
    $a->institution = $org->fullname;
    $a->username = $user->username;
    $a->link = (string) new moodle_url('/');

    $subject = get_string('assignnotification_subject','block_manager_dashboard', $program->fullname);
    $body = get_string('assignnotification_body','block_manager_dashboard', $a);
    $from = manager_dashboard_get_support_user();
    $result = \email_to_user($user, $from, $subject, $body);

    $subject = get_string('assignnotification_managercopy','block_manager_dashboard').' '.$subject;
    $body = get_string('assignnotification_managercopy_body','block_manager_dashboard', $a->userfullname)."\n".$body;

    $managerid = $jobassignment->managerid;
    $manager = $DB->get_record('user',['id'=>$managerid]);
    $result = \email_to_user($manager, $from, $subject, $body);
}

// added own function because core function sometimes is weird (wrong random name)
function manager_dashboard_get_support_user() {
    global $CFG;
    $supportuser = get_admin(); // we need a dummy user
    $supportuser->id = \core_user::SUPPORT_USER;
    $supportuser->email = $CFG->supportemail;
    if ($CFG->supportname) {
        $supportuser->firstname = $CFG->supportname;
        $supportuser->lastname = "";
    }
    $supportuser->username = 'support';
    $supportuser->maildisplay = '1'; // Show to all.
    // Unset emailstop to make sure support message is sent.
    $supportuser->emailstop = 0;
    return $supportuser;
}

function manager_dashboard_clear_old_grades($pid, $userid) {
    $certifications = block_manager_dashboard::get_available_certifications();
    $certification = false;
    foreach($certifications as  $c) {
        if ($c['pid'] == $pid) $certification = $c;
    }
    if(!$c) return false;

    list ($certcompletion, $progcompletion) = certif_load_completion($pid, $userid, false);
    if($certcompletion) {
        reset_certifcomponent_completions($certcompletion);
    }
}

function pr($value, $die=false) {
	echo "<pre>";
	print_r($value);
	echo "</pre>";

	if ($die) die();
}
