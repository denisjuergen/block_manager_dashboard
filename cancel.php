<?php
require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once("$CFG->libdir/formslib.php");
require_once(dirname(__FILE__).'/cancel_form.php');


$certifid = required_param('certifid', PARAM_INT);
$userid = required_param('userid', PARAM_INT);
$returnurl =  required_param('returnurl', PARAM_URL);

$syscontext = context_system::instance();
require_login();

require_capability('block/manager_dashboard:allowcancel', $syscontext);
$PAGE->set_context($syscontext);
$PAGE->set_url('/blocks/manager_dashboard/cancel.php', ['userid'=>$userid,'certifid'=>$certifid]);

global $DB;

$mform = new cancel_enrolment_form();
if ($mform->is_cancelled()) {
    //Handle form cancel operation, if cancel button is pressed
    redirect($returnurl);
} else if ($fromform = $mform->get_data()) {
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('unenrol', 'core_enrol'));
    $data = $mform->get_data();
    // Unapprove for all courses in certif
    $courses = find_courses_for_certif($data->certifid);
    foreach($courses as $course) {   
        \availability_managerapproval\condition::unapprove($userid, $course->id);
    }
    echo get_string('managerapproval_reverted','block_manager_dashboard').'<br/>';

    $records = $DB->get_records_sql("SELECT sessions.id, signups.* FROM {facetoface} as facetoface
        LEFT JOIN {facetoface_sessions} as sessions ON sessions.facetoface = facetoface.id
        LEFT JOIN {facetoface_signups} as signups ON signups.sessionid = sessions.id 
        WHERE signups.userid = :userid
        AND facetoface.course IN (:courseids)",
    ['userid'=>$userid, 'courseids' => implode(',',array_keys($courses))]);
    
    if(count($records)) {
        require_once($CFG->dirroot.'/mod/facetoface/lib.php');
        foreach($records as $record) {
            $session = $DB->get_record('facetoface_sessions',['id'=>$record->sessionid]);
            $session->sessiondates = facetoface_get_session_dates($session->id);
            facetoface_user_cancel($session, $userid);
            echo get_string('employee_unenrolled_from_session','block_manager_dashboard').'<br/>';
        }
    }
    echo get_string('employee_unenrolled','block_manager_dashboard').'<br/>';
    echo '<a href="'.$data->returnurl.'" class="btn btn-primary">Ok</a>';

} else {
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('unenrol', 'core_enrol'));
    $a = new stdClass();
    $user = $DB->get_record('user',['id'=>$userid]);
    $a->user_fullname = fullname($user);

    $prog = $DB->get_record('prog',['certifid'=>$certifid]);
    $a->certif_name = $prog->fullname;
    echo '<p>'.get_string('unenrol_sure','block_manager_dashboard', $a).'</p>';
    $mform->set_data(['userid'=>$userid, 'certifid'=>$certifid, 'returnurl'=>$returnurl]);
    $mform->display();
}

echo $OUTPUT->footer();
