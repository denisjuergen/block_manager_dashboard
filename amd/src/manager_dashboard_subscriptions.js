/**
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Alastair Munro <alastair.munro@totaralearning.com>
 * @package block_current_learning
 */

define(['jquery',
        'core/templates',
        'core/notification'
    ], function($, templates, notification) {

    var showSuccess = function(text) {
         notification.addNotification({
             message: text,
             type: "success"
         });
    };

    /**
     * Init function for the block
     *
     * @param {Object} blockData
     */
     var init = function(blockData) {

       function handleSubmit(e) {
         e.preventDefault();
         var form = $(this);
         var rowid = form.data('rowid');
         var div = form.parent();
         var formdata = form.serializeArray();
         var newtitle = formdata[0]['value'];

         $.post(blockData.ajaxurl, {
             action: 'changeponame',
             newtitle: newtitle,
             rowid: rowid,
             userid: -1
         }, function(data) {
             if (data.result == "OK") {
                showSuccess(data.message);
                div.html(newtitle);
                templates.renderIcon('edit').done(function (html) {
                    var icon = $('<a href="#" >' + html + '</a>');
                    icon.click(handleClick);
                    div.append(icon);
                });
                div.append('<span style="display:none;" class="rowid">' + rowid + '</span>');
             }
         });

       }

       function handleClick(e) {
           e.preventDefault();
           var element = $(this);
           var cell = element.parent();
           var rowidspan = cell.find('.rowid');
           var rowid = rowidspan.text();
           rowidspan.remove();
           var form = $('<form action="#" data-rowid="' + rowid + '">' +
                        '<input name="title" type="text" class="titleeditor" autocomplete="off" maxlength="255" pattern="[a-zA-Z0-9-,._() ]*" value="' + cell.text() + '">' +
                      '</form>');
           form.submit(handleSubmit);
           cell.html(form);
       }

       templates.renderIcon('edit').done(function (html) {
           var icon = $('<a href="#">' + html + '</a>');
           icon.click(handleClick);
           $('tbody .rb_source_assignments_ponumber').append(icon);
       });

     };

    return {
        init: init,
    };
});
