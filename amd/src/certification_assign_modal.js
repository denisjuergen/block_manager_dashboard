/**
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Alastair Munro <alastair.munro@totaralearning.com>
 * @package block_current_learning
 */

define(['jquery',
        'core/templates',
        'core/notification',
        'core/str',
        'core/log'
    ], function($, templates, notification, mdlstr, log) {

    var _blockData = null;

    // This gets set on init.
    var instanceselector;

    var contact_support_string = "";

    // Used when parent category is not yet added.
    var _modal_category_queue = [];

    /**
     * Show an success message to the user.
     * @param {String} text
     */
    var showSuccess = function(text, userid) {
        var context = {
                message: text,
                success: true,
                userid: userid
        };
        templates.render('block_manager_dashboard/notification', context).then(function(html) {
            $('.certification_assign_notifications.userid-'+userid).append(html);
        });
   };

   /**
    * Show an failed message to the user.
    * @param {String} text
    */
   var showFailed = function(text) {
       notification.addNotification({
           message: text,
           type: "failed"
       });
   };

    /**
     * Initialises collapse using Bootstrap JS.
     */
    var initDetailCollapse = function() {
        $('.collapse').on('show.bs.collapse', function(e) {
            $(".detaillink[data-target='#" + e.target.id + "'] span").removeClass('fa-caret-right').addClass('fa-caret-down');
        });

        $('.collapse').on('hide.bs.collapse', function(e) {
            $(".detaillink[data-target='#" + e.target.id + "'] span").removeClass('fa-caret-down').addClass('fa-caret-right');
        });
    };

    var checkAssignButtonEnabled = function(e) {
        var enable = false;
        var agreecheckbox = $(e.target).closest('div.cert').find('input.understand').first();
        if (agreecheckbox.prop("checked")) {
            enable = true;
        } else {
            enable = false;
        }

        if (agreecheckbox.closest("form").find('input[name=session]').length > 0) {
            var seminarval = agreecheckbox.closest("form").find('input[name=session]:checked').val();
            if (enable && typeof seminarval !== "undefined") {
                enable = true;
            } else {
                enable = false;
            }
        }

        if (enable) {
            agreecheckbox.closest("form").find('.assignuser').removeClass("disabled");
        } else {
            agreecheckbox.closest("form").find('.assignuser').addClass("disabled");
        }
    };

    var initModalCertificationEvents = function() {
        $(instanceselector + ' #assignModal .cert')
        .off('show.bs.collapse')
        .on('show.bs.collapse', function(e) {
            var data = {
                action: 'getcertificationdates',
                certificationid: $(e.target).data("certid"),
                userid: $(instanceselector + ' .hiddenuserid').attr("value")
            };

            $.get(_blockData.ajaxurl, data, function(data) {
                if (data.result !== "OK") {
                    showFailed(data.message);
                    return;
                }
                if(data.contact == true) {
                    $(e.target).find('.pickadate').html(
                        '<span aria-hidden="true" class="flex-icon ft-fw ft fa-envelope-o"></span> ' +
                        contact_support_string
                        );
                } else if (data.iselearning == true) {
                    $(e.target).find('.pickadate').
                        html('<span aria-hidden="true" class="flex-icon ft-fw ft ft-archive"></span> E-learning');
                    $(e.target).find('.coursename').html(data.sessionname);
                    $(e.target).closest('.cert').find(".assignuser").slideDown();
                } else {
                    var resulttable = $('<table class="table table-striped"></table>');
                    var tbody = $('<tbody/>');
                    for (var session in data.sessions) {
                        if (!data.sessions.hasOwnProperty(session)) {
                            continue;
                        }
                        var row = $('<tr/>');
                        var asession = data.sessions[session];
                        var style = "";
                        if (asession.isfull) {
                            style = ' class="full" disabled ';
                        }
                        row.append('<td><input type="radio" name="session" ' +
                            style + ' value="' + asession.id + '"/></td>');

                        var days = [];
                        var dates = [];
                        var locations = [];

                        for (var date in asession.dates) {
                            if (!asession.dates.hasOwnProperty(date)) {
                                continue;
                            }
                            var dateexploded = asession.dates[date].start.split(',');
                            days.push(dateexploded[0]);
                            dates.push(dateexploded[1]);
                            locations.push(asession.dates[date].room);
                        }

                        var locationcell = '<td>'+locations.join('<br/>')+'</td>';
                        row.append(locationcell);
                        var daycell = '<td>'+days.join('<br/>')+'</td>';
                        row.append(daycell);
                        var datecell = '<td>'+dates.join('<br/>')+'</td>';
                        row.append(datecell);

                        if (asession.isfull) {
                            row.append('<td><span class="label label-danger">' + asession.freetext + '</span></td>');
                        } else if (asession.free > 3) {
                            row.append('<td><span class="label label-success">' + asession.freetext + '</span></td>');
                        } else {
                            row.append('<td><span class="label label-warning">' + asession.freetext + '</span></td>');
                        }

                        $(e.target).find('input[name=session]').click(checkAssignButtonEnabled);
                        tbody.append(row);
                    }
                    resulttable.append(tbody);
                    $(tbody).find('td').click(function(){
                        $(this).closest('table').find('input[name=session]').attr('checked', false);
                        $(this).closest('tr').find('input[name=session]').attr('checked', true);
                    });
                    $(e.target).find('.pickadate').append(resulttable);
                    $(e.target).find('.coursename').html(data.sessionname);
                    $(e.target).closest('.cert').find(".assignuser").slideDown();
                }
                $(e.target).find('.loading').slideUp();
                $(e.target).find('.pickadate').slideDown();

            });
        });

        $(instanceselector + ' #assignModal .collapse.cert')
        .off('hidden.bs.collapse')
        .on('hidden.bs.collapse', function(e) {
            $(e.target).find('.loading').show();
            $(e.target).find('.pickadate').hide().empty();
        });

        // toggle button on/off on checkbox state
        $(instanceselector + ' #assignModal input.understand:checkbox')
        .off('click')
        .click(checkAssignButtonEnabled);

        // Listen to assign button
        $(instanceselector + ' #assignModal .assignuser')
        .off('click')
        .click(function() {
            $(this).addClass('disabled');
            var inputs = $(this).closest("form").find("input");
            var data = {
                action: 'assign'
            };
            inputs.each(function() {
                data[$(this).attr("name")] = $(this).val();
            });
            data.session = $('input[name=session]:checked').val();
            $.post(_blockData.ajaxurl, data, function(data) {
                if (data.result == "OK") {
                    $('#block_manager_dashboard-employee-'+data.id+'-certifications.current tbody tr.nocurrent').remove();
                    $('#block_manager_dashboard-employee-'+data.id+'-certifications.current tbody').append(data.newrow);

                    $('#block_manager_dashboard-approvalrequired td a').each(function() {
                        if(
                            $(this).data("userid")==data.id &&
                            $(this).data("program")==data.programid
                        ) {
                            $(this).replaceWith("<span>Ingeschreven.</span>");
                        }
                    });
                    showSuccess(data.message, data.id);
                    $(instanceselector + ' #assignModal').modal('hide');
                } else {
                    showFailed(data.message);
                }
                $(instanceselector + ' #assignModal').modal('hide');
            });
        });
    };

    var appendCertification = function(certification, html, js) {
        var selector = instanceselector+" #assignModal #categoryAccordion #collapse"+certification.category;
        templates.appendNodeContents(selector, html, js);
        initModalCertificationEvents(instanceselector);
    };

    var retryAppendCertification = function() {
        //console.log("retryAppendCertification: "+_modal_category_queue.length+" remaining");
        // try appending previously queued categories
        for (var k = 0; k < _modal_category_queue.length; k++) {
            var queuedcat = _modal_category_queue.shift();
            appendCategory(queuedcat.category, queuedcat.html, queuedcat.js);
        }
    };

    var appendCategory = function(category, html, js) {
        var selector = instanceselector+" #assignModal #categoryAccordion #collapse"+category.parent;
        if(category.parent==0) {
            selector = instanceselector+" #assignModal #categoryAccordion";
        }
        // Check if parent category already exists. If not append to queue.
        if($(selector).length<1) {
            var cat = {
                category: category,
                html: html,
                js: js
            };

            _modal_category_queue.push(cat);
            return;
        }
        templates.appendNodeContents(selector, html, js);

        for (var i = 0; i < _blockData.availablecertifications.length; i++) {
            if(category.id == _blockData.availablecertifications[i].category) {
                templates.render('block_manager_dashboard/certification_assign_modal_cat_cert',
                _blockData.availablecertifications[i])
                .then(
                    appendCertification.bind(null,_blockData.availablecertifications[i])
                );
            }
        }

        setTimeout(retryAppendCertification, 500);
    };

    var initModalData = function(categories) {
        for (var i = 0; i < categories.length; i++) {
            templates.render('block_manager_dashboard/certification_assign_modal_cat', categories[i])
                .then(appendCategory.bind(null,categories[i]));
        }
    };

    var initModalScript = function() {
        // fill the correct fields on show of dialog
        $(instanceselector + ' #assignModal').on('show.bs.modal', function(e) {
            $(instanceselector + ' .userfullname').text($(e.relatedTarget).data("fullname"));
            $(instanceselector + ' .hiddenuserid').attr("value", $(e.relatedTarget).data("userid"));
            $(instanceselector + ' #assignModal input.understand:checkbox').prop("checked", false);

            // Als 't knopje een certification meegeeft, zoek hem op in de boom en klap hem (en parents) open
            if($(e.relatedTarget).data("program")!=undefined) {
                var element = $(instanceselector+" #assignModal #collapseCert"+$(e.relatedTarget).data("program"));
                element.collapse("show");
                element.parents(".collapse").collapse("show");
            }
        });

        $(instanceselector + ' #assignModal').on('hide.bs.modal', function() {
            $(instanceselector + ' #assignModal .collapse').collapse('hide');
        });
    };

    /**
     * Init function for the block
     *
     * @param {Object} blockData
     */
    var init = function(blockData) {
        instanceselector = '#inst' + blockData.instanceid.toString();

        var bsjavascript = true;
        if (!$.fn.collapse) {
            log.debug('Current learning block requires Bootstrap 3 JavaScript, please include it or find your own solution.');
            bsjavascript = false;
        }

        _blockData = blockData;

        var blockTemplates = [
            'block_manager_dashboard/certification_assign_modal_cat',
            'block_manager_dashboard/certification_assign_modal_cat_cert',
            'block_manager_dashboard/success_checkmark'
        ];

        // Preload Templates to cache them on the client
        // preventing a delay when a user first performs an
        // action that required the templates to be re-rendered.
        blockTemplates.forEach(function(template) {
            templates.render(template, {});
        });

        // if (bsjavascript) {
        //     initDetailCollapse();
        // }

        initModalData(blockData.categories);
        initModalScript();
        initModalCertificationEvents();

        var contactstring = mdlstr.get_string('contact_support', 'block_manager_dashboard');
        $.when(contactstring).done(function(localizedContactString) {
            contact_support_string = localizedContactString;
       });
    };

    return {
        init: init
    };
});
