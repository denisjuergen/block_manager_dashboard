/**
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Alastair Munro <alastair.munro@totaralearning.com>
 * @package block_current_learning
 */

define(['jquery',
        'core/templates',
        'core/notification',
        'core/str',
        'core/log'
    ], function($, templates, notification, mdlstr, log) {

    var _blockData = null;

    // This gets set on init.
    var instanceselector;

    var items_per_page = 100;

    /**
     * Initialises collapse using Bootstrap JS.
     */
    var initCollapseSelf = function() {
      $('.block_manager_dashboard-row-item').click( function() {
          var target = $(this).data('target');

          if ( $(target).hasClass('in') ) {
            $(target).removeClass('in');
            $(".detaillink[data-target='" + target + "'] span").removeClass('fa-caret-down').addClass('fa-caret-right');
          } else {
            $(target).addClass('in');
            $(".detaillink[data-target='" + target + "'] span").removeClass('fa-caret-right').addClass('fa-caret-down');
          }

      });
    }

    var initCollapse = function() {

    };

    /**
     * Show an success message to the user.
     * @param {String} text
     */
    var showSuccess = function(text) {
      if (text != '' && text != null) {
        alert(text);
      }
      // notification.addNotification({
      //     message: text,
      //     type: "success"
      // });
    };

    /**
     * Show an failed message to the user in a specified div.
     * @param {String} text
     * @param {Object} target
     */
    var showFailedInDiv = function(text, target) {
        $(target).find("div.alert").slideUp();
        $(target).append(
            '<div class="alert alert-danger alert-dismissible col-md-12" role="alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">×</span></button>' +
            text + '</div>'
        );
    };

    /**
     * Show an failed message to the user.
     * @param {String} text
     */
    var showFailed = function(text) {
        // notification.addNotification({
        //     message: text,
        //     type: "failed"
        // });
    };

    var initSortLinks = function(instanceselector, data) {
        var element;
        if (data.field == 'firstname') {
            element = $(instanceselector + ' a.firstname');
        } else {
            element = $(instanceselector + ' a.lastname');
        }
        if (data.order == -1) {
            element.addClass('asc');
        } else {
            element.addClass('desc');
        }
    };

    /**
     * Initialises search function.
     */
    var initSearchForm = function() {
        $('#searchname').keypress(function(e) {
            if (e.which == 13) {
              $('form#manager_dashboard-searchform').submit();
              e.preventDefault();
              return false;
            }
            return true;
        });

        $('#searchorganization').on('change', function() {
            $(this).closest('form').submit();
        });

        $('#showsuspended').on('change', function() {
            $(this).closest('form').submit();
        });

    };

    // var initDatePicker = function() {
    //     M.totara_core.build_datepicker(null, '.birthdateselector',
    //         M.util.get_string('datepickerlongyeardisplayformat', 'totara_core'));
    // };

    var exportList = function() {
      // var data = {
      //   userid: -1,
      //   action: 'exportlist'
      // };
      // // $.get(_blockData.ajaxurl, data, function(data) {
        document.location.href = _blockData.ajaxurl + '?userid=-1&action=exportlist';
      // });
    };

    var addNewEmployee = function() {
        var form = $(this).closest('.modal').find('form').first();
        form.find('.has-error').removeClass('has-error');

        var inputs = form.find(":input");
        var data = {
            userid: -1,
            action: 'adduser'
        };
        inputs.each(function() {
            data[$(this).attr("id")] = $(this).val();
        });
        var checkboxes = form.find("input:checkbox");
        checkboxes.each(function() {
          if ($(this).prop('checked')) {
            data[$(this).attr("id")] = 1;
          } else {
            data[$(this).attr("id")] = 0;
          }
        });
        form.find(":input").prop('disabled', true);
        $(this).html($(this).html() + '<span aria-hidden="true" data-flex-icon="loading" ' +
            'class="flex-icon ft-fw ft fa-spinner fa-pulse"></span>');

        $.post(_blockData.ajaxurl, data, function(data) {
            if (data.result == "OK") {
                alert('Thank you! Account is created and email sent to the user');
                window.location.href = window.location.href;
            } else {
                var errortext = "";
                for (var key in data.errors) {
                    if (!data.errors.hasOwnProperty(key)) {
                        continue;
                    }
                    form.find('#' + key).parent().addClass('has-error');
                    errortext += key + ": " + data.errors[key] + "</br>";
                }
                showFailedInDiv(errortext, $(instanceselector + " #newEmployeeModal").find('.alerts').first());
            }
            form.find(":input").prop('disabled', false);
            $(instanceselector + ' button#addNewEmployee').find("span.flex-icon").remove();
        });
    };

    var initManagerPicker = function(organizationselect, managerselect) {
        $(organizationselect).on('change', function(e) {
            $(managerselect).html('').prop('disabled', true);
            $.post(_blockData.ajaxurl, {
                action: 'getmanagers',
                orgid: $(e.target).val(),
                userid: -1
            }, function(data) {
                if (data.result == "OK") {
                    data.managers.forEach(function(value){
                        $(managerselect).append('<option value="'+
                            value.manager_id+'">'+value.manager_fullname+'</option>');
                    });
                    $(managerselect).prop('disabled', false);
                }
            });
        });
    };

    /**
     * Initialises buttons in edit profile
     * @param {String} instanceselector
     */
    var initUserButtons = function(instanceselector) {
        $(instanceselector + " a.suspenduser").click(function() {
            var userid = $(this).data('target');
            $(instanceselector + " tr#row-details-employee-" + userid + " :input").prop('disabled', true);
            $(this).find("span.flex-icon").
                replaceWith('<span aria-hidden="true" data-flex-icon="loading" '
                    + 'class="flex-icon ft-fw ft fa-spinner fa-pulse"></span>');

            $.post(_blockData.ajaxurl, {
                action: 'suspend',
                userid: userid
            }, function(data) {
                if (data.result == "OK") {
                    showSuccess(data.message);
                   $(instanceselector + " tr#row-summary-employee-" + data.id).slideUp("slow");
                   $(instanceselector + " tr#row-details-employee-" + data.id).slideUp("slow");
                }
            });
        });

        $(instanceselector + " a.unsuspenduser").click(function() {
            var userid = $(this).data('target');
            $(instanceselector + " tr#row-details-employee-" + userid + " :input").prop('disabled', true);
            $(this).find("span.flex-icon").
                replaceWith('<span aria-hidden="true" data-flex-icon="loading" ' +
                    'class="flex-icon ft-fw ft fa-spinner fa-pulse"></span>');

            $.post(_blockData.ajaxurl, {
                action: 'unsuspend',
                userid: userid
            }, function(data) {
                if (data.result == "OK") {
                    showSuccess(data.message);
                   $(instanceselector + " tr#row-summary-employee-" + data.id).slideUp("slow");
                   $(instanceselector + " tr#row-details-employee-" + data.id).slideUp("slow");
                }
            });
        });

        $(instanceselector + " a.resetuserpassword").click(function() {
            var userid = $(this).data('target');
            $(this).find("span.flex-icon").
                replaceWith('<span aria-hidden="true" data-flex-icon="loading" ' +
                'class="flex-icon ft-fw ft fa-spinner fa-pulse"></span>');

            $.post(_blockData.ajaxurl, {
                action: 'resetuserpassword',
                userid: userid
            }, function(data) {
                if (data.result == "OK") {
                    showSuccess(data.message);
                } else {
                    showFailed(data.message);
                }
                $(instanceselector + " tr#row-details-employee-" + data.id + " a.resetuserpassword span.flex-icon").
                    replaceWith('<span aria-hidden="true" data-flex-icon="key" class="flex-icon ft-fw ft fa-key"></span>');
            });
        });

        $(instanceselector + " input.update-user").click(function() {
            var userid = $(this).data('target');
            $(instanceselector + " tr#row-details-employee-" + userid + " :input").prop('disabled', true);
            var form = $(this).closest("form");
            form.find('.has-error').removeClass('has-error');
            $(this).html($(this).html + '<span aria-hidden="true" data-flex-icon="loading" ' +
                'class="flex-icon ft-fw ft fa-spinner fa-pulse"></span>');

                var checkboxes = form.find("input:checkbox");
                var data = {
                    action: 'updateuser',
                    // username: $(form).find('#username' + userid).first().val(),
                    firstname: $(form).find('#firstname' + userid).first().val(),
                    lastname: $(form).find('#lastname' + userid).first().val(),
                    email: $(form).find('#email' + userid).first().val(),
                    country: $(form).find('#country' + userid).first().val(),
                    // birthdate: $(form).find('#birthdate' + userid).first().val(),
                    organization: $(form).find('#organization' + userid).first().val(),
                    department: $(form).find('#department' + userid).first().val(),
                    managerid: $(form).find('#manager' + userid).first().val(),
                    contracttype: $(form).find('#contracttype' + userid).first().val(),
                    language: $(form).find('#language' + userid).first().val(),
                    userid: userid,
                };
                checkboxes.each(function() {
                  if ($(this).prop('checked')) {
                    data[$(this).attr("id")] = 1;
                  } else {
                    data[$(this).attr("id")] = 0;
                  }
                });
                $.post(_blockData.ajaxurl, data, function(data) {
                // $.post(_blockData.ajaxurl, {
                //     action: 'updateuser',
                //     // username: $(form).find('#username' + userid).first().val(),
                //     firstname: $(form).find('#firstname' + userid).first().val(),
                //     lastname: $(form).find('#lastname' + userid).first().val(),
                //     email: $(form).find('#email' + userid).first().val(),
                //     country: $(form).find('#country' + userid).first().val(),
                //     // birthdate: $(form).find('#birthdate' + userid).first().val(),
                //     organization: $(form).find('#organization' + userid).first().val(),
                //     managerid: $(form).find('#manager' + userid).first().val(),
                //     contracttype: $(form).find('#contracttype' + userid).first().val(),
                //
                //     userid: userid,
                // }, function(data) {
                if (data.result == "OK") {
                    var row = $(instanceselector + " tr#row-summary-employee-" + userid);
                    $(row).find("td:nth-child(2)").first().html(data.fullname);
                    $(row).find("td:nth-child(3)").first().html(data.username);
                    $(row).find("td:nth-child(4)").first().html(data.organization);
                    $(row).find("td:nth-child(5) a").first().attr('href', 'mailto:'+data.email);
                    showSuccess(data.message);
                } else {
                    var errortext = "";
                    for (var key in data.errors) {
                        if (!data.errors.hasOwnProperty(key)) {
                            continue;
                        }
                        $(form).find('#' + key + userid).parent().addClass('has-error');
                        errortext += key + ": " + data.errors[key] + "</br>";
                    }
                    showFailed(errortext);
                }
                $(instanceselector + " tr#row-details-employee-" + userid + " :input").prop('disabled', false);
                $(instanceselector + " tr#row-details-employee-" + data.id + "input.update-user span.flex-icon").remove();
            });
        });

        if(_blockData.can_make_manager) {
            $(instanceselector + " a.make_manager").click(function() {
                var userid = $(this).data('target');
                $(this).find("span.flex-icon").
                    replaceWith('<span aria-hidden="true" data-flex-icon="loading" ' +
                    'class="flex-icon ft-fw ft fa-spinner fa-pulse"></span>');
                $.post(_blockData.ajaxurl, {
                    action: 'makemanager',
                    userid: userid,
                    newrole: $(this).data('newrole')
                }, function(data) {
                    if (data.result == "OK") {
                        showSuccess(data.message);
                        window.location.href = window.location.href;
                    } else {
                        showFailed(data.message);
                    }
                    $(instanceselector + " tr#row-details-employee-" + userid + " a.make_manager").html(
                        '<span aria-hidden="true" data-flex-icon="rating-star" class="flex-icon ft-fw ft fa-star-half-o"></span> ' +
                            data.newtext);
                    $(instanceselector + " tr#row-details-employee-" + userid + " a.make_manager").data("newrole", data.newrole);
                });
            });

            $(instanceselector + " .block_manager_dashboard-row-item .collapse").each(function(index, element) {
                var orgpicker = $(element).find(".organizationpicker").first();
                var managerpicker = $(element).find(".managerpicker").first();
                initManagerPicker(orgpicker, managerpicker);
            });
        }
    };

    var filter_data_for_paging = function(data, page) {
        // Get the current page.
        var currentPage = parseInt($(instanceselector+' .pagination li.active a').attr('data-page'));
        var newPage = null;

        if (page == 'next') {
            newPage = currentPage + 1;
        } else if (page == 'prev') {
            newPage = currentPage - 1;
        } else {
            newPage = page;
        }

        var numOfItems = data.employees.length;
        var numOfPages = Math.ceil(numOfItems / items_per_page);

        var result = {};
        var pageStart = ((newPage - 1) * items_per_page) + 1;

        // Get the 10 items assigned to the page.
        result.employees = data.employees.slice(pageStart - 1, (pageStart - 1) + items_per_page);
        result.availableorganisations = _blockData.availableorganisations;
        result.can_make_manager = _blockData.can_make_manager;

        // Update the paging data to reflect the page change.
        var pagingData = data.pagination;
        if (newPage > 1) {
            pagingData.previousclass = '';
        } else {
            pagingData.previousclass = 'disabled';
        }

        // If we are at the end then disable the next page button.
        if (newPage >= numOfPages) {
            pagingData.nextclass = 'disabled';
        } else {
            pagingData.nextclass = '';
        }

        // If we are at the start disable the previous page button.
        pagingData.pages.forEach(function(p) {
            if (p.page == newPage) {
                p.active = 'active';
            } else {
                p.active = '';
            }
        });

        // Update display text.
        var strData = {};
        var stringDeferred = $.Deferred();
        strData.start = ((newPage - 1) * items_per_page) + 1;
        var pageEnd = null;
        if (data.pagination.totalitems < newPage * items_per_page) {
            pageEnd = data.pagination.totalitems;
        } else {
            pageEnd = (newPage * items_per_page);
        }
        strData.end = pageEnd;
        strData.total = data.pagination.totalitems;
        mdlstr.get_string('displayingxofx', 'block_current_learning', strData)
            .done(function(paginationString) {
                pagingData.text = paginationString;
                result.pagination = pagingData;
                stringDeferred.resolve(result);
            }).fail(function(err) {
                stringDeferred.reject(err);
            });

        return stringDeferred.promise();
    };

    var updateOrganisationEditField = function() {
        $("select.organizationpicker").each(function() {
            var value = $(this).data('value');
            $(this).find('option[value="' + value + '"]').attr('selected', 'selected');
        });
        $("select.contracttype").each(function() {
            var value = $(this).data('value');
            $(this).find('option[value="' + value + '"]').attr('selected', 'selected');
        });
    };

    var updateManagerEditField = function() {
        $("select.managerpicker").each(function() {
            var value = $(this).data('value');
            $(this).find('option[value="' + value + '"]').attr('selected', 'selected');
        });
    };

    var initNewUserManagerPicker = function() {
        $(instanceselector+' #newEmployeeModal #manager').prop('disabled', false);
        initManagerPicker(
            $(instanceselector+' #newEmployeeModal #organization'),
            $(instanceselector+' #newEmployeeModal #manager')
        );
    };

    /**
     * Initialises handlers for pagination.
     */
    var initPaginationHandlers = function() {
        $(instanceselector+' .pagination').on('click', 'a', function(e) {
            e.preventDefault();

            var anchor = $(this); // The <li>
            var parent = anchor.parent();

            // If the button clicked is disabled then return.
            if (parent.hasClass('disabled')) {
                return false;
            }

            var page = anchor.attr('data-page');

            filter_data_for_paging(_blockData, page).done(function(filteredData) {
                templates.render('block_manager_dashboard/employee_table', filteredData).done(function(rendered) {
                    $(instanceselector+' #block_manager_dashboard-employee').replaceWith(rendered);
                    updateOrganisationEditField();
                    updateManagerEditField();
                    initUserButtons(instanceselector);
                    initCollapseSelf();
                }).fail(function(error) {
                    notification.exception(error);
                });

                // Re-render the footer.
                templates.render('block_manager_dashboard/paging', filteredData).done(function(rendered) {
                    $(instanceselector+' .panel-footer').replaceWith(rendered);
                    initPaginationHandlers();
                }).fail(function(error) {
                    notification.exception(error);
                });
            });
        });
    };

    /**
     * Init function for the block
     *
     * @param {Object} blockData
     */
    var init = function(blockData) {

        instanceselector = '#inst' + blockData.instanceid.toString();

        var bsjavascript = true;
        if (!$.fn.collapse) {
            log.debug('Current learning block requires Bootstrap 3 JavaScript, please include it or find your own solution.');
            bsjavascript = false;
        }

        _blockData = blockData;

        var blockTemplates = [
            'block_manager_dashboard/employee_table',
            'block_manager_dashboard/paging',
            'block_manager_dashboard/manage_employee_row',
            'block_manager_dashboard/employee_form',
            'block_manager_dashboard/organisation_picker',
        ];

        // Preload Templates to cache them on the client
        // preventing a delay when a user first performs an
        // action that required the templates to be re-rendered.
        blockTemplates.forEach(function(template) {
            templates.render(template, {});
        });

        if (bsjavascript) {
            // initCollapse();
        }

        initCollapseSelf();

        $(instanceselector + ' button#addNewEmployee').click(addNewEmployee);
        $('button#exportList').click(exportList);

        initUserButtons(instanceselector);
        updateOrganisationEditField();
        updateManagerEditField();
        initSearchForm(instanceselector);

        var showsuspended = $(instanceselector + " #showsuspended").first().attr("checked");
        if (typeof showsuspended !== "undefined" && showsuspended == "checked") {
            $(instanceselector + " tr :input").prop('disabled', true);
        }

        initSortLinks(instanceselector, blockData.sortorder);
        initPaginationHandlers();

        // initDatePicker();
        initNewUserManagerPicker();
    };

    return {
        init: init
    };
});
