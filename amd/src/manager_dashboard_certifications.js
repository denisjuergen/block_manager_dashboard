/**
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Alastair Munro <alastair.munro@totaralearning.com>
 * @package block_current_learning
 */

define(['jquery',
        'core/templates',
        'core/notification',
        'core/str',
        'core/log'
    ], function($, templates, notification, mdlstr, log) {

    var _blockData = null;

    // This gets set on init.
    var instanceselector;

    var items_per_page = 10;

    /**
     * Initialises collapse using Bootstrap JS.
     */
    var initDetailCollapse = function() {
        $('.collapse').on('show.bs.collapse', function(e) {
            $(".detaillink[data-target='#" + e.target.id + "'] span").removeClass('fa-caret-right').addClass('fa-caret-down');
        });

        $('.collapse').on('hide.bs.collapse', function(e) {
            $(".detaillink[data-target='#" + e.target.id + "'] span").removeClass('fa-caret-down').addClass('fa-caret-right');
        });
    };

    /**
     * Initialises search function.
     */
    var initSearchForm = function() {
        $('#searchname').keypress(function(e) {
            if (e.which == 13) {
              $('form#manager_dashboard-searchform').submit();
              e.preventDefault();
              return false;
            }
            return true;
        });

        $('#searchorganization').on('change', function() {
            $(this).closest('form').submit();
        });

        $('#showsuspended').on('change', function() {
            $(this).closest('form').submit();
        });

    };

    var initSortLinks = function(data) {
        var element;
        if (data.field == 'firstname') {
            element = $(instanceselector + ' a.firstname');
        } else {
            element = $(instanceselector + ' a.lastname');
        }
        if (data.order == -1) {
            element.addClass('asc');
        } else {
            element.addClass('desc');
        }
    };

    var filter_data_for_paging = function(data, page) {
        // Get the current page.
        var currentPage = parseInt($(instanceselector+' .pagination li.active a').attr('data-page'));
        var newPage = null;

        if (page == 'next') {
            newPage = currentPage + 1;
        } else if (page == 'prev') {
            newPage = currentPage - 1;
        } else {
            newPage = page;
        }

        var numOfItems = data.employees.length;
        var numOfPages = Math.ceil(numOfItems / items_per_page);

        var result = {};
        var pageStart = ((newPage - 1) * items_per_page) + 1;

        // Get the 10 items assigned to the page.
        result.employees = data.employees.slice(pageStart - 1, (pageStart - 1) + items_per_page);
        result.hasemployees = true;

        // Update the paging data to reflect the page change.
        var pagingData = data.pagination;
        if (newPage > 1) {
            pagingData.previousclass = '';
        } else {
            pagingData.previousclass = 'disabled';
        }

        // If we are at the end then disable the next page button.
        if (newPage >= numOfPages) {
            pagingData.nextclass = 'disabled';
        } else {
            pagingData.nextclass = '';
        }

        // If we are at the start disable the previous page button.
        pagingData.pages.forEach(function(p) {
            if (p.page == newPage) {
                p.active = 'active';
            } else {
                p.active = '';
            }
        });

        // Update display text.
        var strData = {};
        var stringDeferred = $.Deferred();
        strData.start = ((newPage - 1) * items_per_page) + 1;
        var pageEnd = null;
        if (data.pagination.totalitems < newPage * items_per_page) {
            pageEnd = data.pagination.totalitems;
        } else {
            pageEnd = (newPage * items_per_page);
        }
        strData.end = pageEnd;
        strData.total = data.pagination.totalitems;
        mdlstr.get_string('displayingxofx', 'block_current_learning', strData)
            .done(function(paginationString) {
                pagingData.text = paginationString;
                result.pagination = pagingData;
                stringDeferred.resolve(result);
            }).fail(function(err) {
                stringDeferred.reject(err);
            });

        return stringDeferred.promise();
    };

    /**
     * Initialises handlers for pagination.
     */
    var initPaginationHandlers = function() {
        $(instanceselector+' .pagination').on('click', 'a', function(e) {
            e.preventDefault();

            var anchor = $(this); // The <li>
            var parent = anchor.parent();

            // If the button clicked is disabled then return.
            if (parent.hasClass('disabled')) {
                return false;
            }

            var page = anchor.attr('data-page');

            filter_data_for_paging(_blockData, page).done(function(filteredData) {
                templates.render('block_manager_dashboard/certifications_content', filteredData).done(function(rendered) {
                    $(instanceselector+' .manager-dashboard-content').replaceWith(rendered);
                }).fail(function(error) {
                    notification.exception(error);
                });

                // Re-render the footer.
                templates.render('block_manager_dashboard/paging', filteredData).done(function(rendered) {
                    $(instanceselector+' .panel-footer').replaceWith(rendered);
                    initPaginationHandlers();
                }).fail(function(error) {
                    notification.exception(error);
                });
            });
        });
    };


    /**
     * Init function for the block
     *
     * @param {Object} blockData
     */
    var init = function(blockData) {
        instanceselector = '#inst' + blockData.instanceid.toString();

        var bsjavascript = true;
        if (!$.fn.collapse) {
            log.debug('Current learning block requires Bootstrap 3 JavaScript, please include it or find your own solution.');
            bsjavascript = false;
        }

        _blockData = blockData;

        var blockTemplates = [
            'block_manager_dashboard/paging',
            'block_manager_dashboard/certifications_content',
            'block_manager_dashboard/certification_row',
            'block_manager_dashboard/certification_assign',
            'block_manager_dashboard/certification_current',
            'block_manager_dashboard/certification_certified',
            'block_manager_dashboard/certification_expired',
            'block_manager_dashboard/certification_current_row',
        ];

        // Preload Templates to cache them on the client
        // preventing a delay when a user first performs an
        // action that required the templates to be re-rendered.
        blockTemplates.forEach(function(template) {
            templates.render(template, {});
        });

        // if (bsjavascript) {
        //     initDetailCollapse();
        // }

        $("select.organizationpicker").each(function() {
            var value = $(this).data('value');
            $(this).find('option[value="' + value + '"]').attr('selected', 'selected');
        });

        initSearchForm();

        var showsuspended = $(instanceselector + " #showsuspended").first().attr("checked");
        if (typeof showsuspended !== "undefined" && showsuspended == "checked") {
            $(instanceselector + " tr :input").prop('disabled', true);
        }

        initSortLinks(blockData.sortorder);

       initPaginationHandlers();

    };

    return {
        init: init
    };
});
