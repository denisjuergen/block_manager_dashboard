<?php
$string['pluginname'] = 'User Management Dashboard';
$string['dashboard'] = 'User Management Dashboard';
$string['manager_dashboard:addinstance'] = 'Add Management Dashboard block to a page';
$string['manager_dashboard:myaddinstance'] = 'Add Management Dashboard block to my moodle page';
$string['manager_dashboard:allowcancel'] = 'Allow cancellation of enrolment';
$string['manager_dashboard:assignmanager'] = 'Allow assign manager';
$string['manager_dashboard:viewsubscriptions'] = 'Allow viewing tab subscriptions';

$string['cachedef_employees'] = 'This is the description of the cache somedata';

$string['birthdate'] = 'Birthdate';
$string['passwordreset:sent'] = 'Password reset email is sent.';
$string['passwordreset:failed'] = 'Unknown error occured while sending reset email.';

$string['searchorg'] = 'Filter organisation';

$string['no_employees'] = 'You have no employees in your organisation.';
$string['pick_manager'] = '-- Pick a manager --';

$string['assignuser'] = 'Assign user';
$string['assigning'] = 'Assigning <strong class="userfullname"></strong> to the certification <strong>{$a}</strong>.';


$string['export'] = 'Export';
$string['adduser'] = 'Add user';

$string['make_student'] = 'Make student';
$string['make_manager'] = 'Make manager';

$string['free'] = '{$a} places available';
$string['single'] = 'Just one place left';
$string['full'] = 'Fully booked';

$string['next'] = 'Next';
$string['previous'] = 'Previous';

$string["orderrefrence"] = 'Order reference:';

$string["rb_source_assignments_title"] = 'Recent assignments';

$string['understand'] = 'I understand there are financial concequences to this action, and hereby approve of this.';
$string['contact_support'] = 'Please contact support if you would like to take this course.';
$string['about_to_approve'] = 'You are about to approve <span class="fullname"></span> for certification <span class="certificationname"></span>.';

$string['no_current'] = 'No certifications in progress.';
$string['no_certified'] = 'No active certifications.';
$string['no_expired'] = 'No expired certifications.';
$string['no_pending'] = 'No pending certifications.';

$string["unsuspended"] = 'Unsuspended';
$string["assigned"] = 'The user {$a->name} was assigned to the certification {$a->certification}.';
$string['unenrol_sure'] = 'Are you sure you want to unenrol <strong>{$a->user_fullname}</strong> from the course <strong>{$a->certif_name}</strong>?';

$string["approvalrequired"] = "Pending renewals";
$string["subscriptions"] = "Subscriptions";
$string["managerapproval_reverted"] = "Renewal approval reverted";
$string["employee_unenrolled_from_session"] = "Employee unenrolled from session.";
$string["employee_unenrolled"] = "Employee unenrolled.";

$string["invalid_manager_selected"] = "Invalid manager selected for this organisation.";

$string['user_assigned'] = 'User enrolled in certification';
$string['user_updated'] = 'User updated';
$string['availability_condition_updated'] = 'Availability approved';
$string['user_added'] = 'User added';
$string['user_password_reset'] = 'User password resetted';
$string['user_suspended'] = 'User suspended';
$string['user_unsuspended'] = 'User unsuspended';

$string['renew'] = 'Renew';

$string['assignnotification_managercopy'] = 'Kopie van verzonden e-mail:';
$string['assignnotification_managercopy_body'] = '
** TER INFO **

Dit is om te informeren dat {$a} is geboekt voor de volgende cursus en u staat in het systeem als zijn/haar manager.

Als u niet hun Manager bent en denkt dat u deze e-mail per ongeluk hebt ontvangen, beantwoordt u deze e-mail.

Onderstaand het bericht wat naar uw medewerker is verstuurd:
';

$string['assignnotification_subject'] = 'Uw inschrijving op {$a}';
$string['assignnotification_body'] = 'Beste {$a->userfullname},

Deze email is om te bevestigen dat je nu bent ingeschreven door {$a->institution} voor de volgende e-learning:

Deelnemer: {$a->userfullname}
E-learning: {$a->certification}
Bedrijf: {$a->institution}

Als u inlogt op {$a->link} staat uw e-learning voor u klaar. Na het behalen van een voldoende resultaat, kunt u direct het behaalde certificaat downloaden.
(uw gebruikersnaam is: {$a->username})

Mocht u uw wachtwoord zijn vergeten of niet hebben, klik dan bij het login-scherm op het knopje ‘wachtwoord vergeten’. Er wordt dan direct een nieuw wachtwoord gegenereerd.
Let op! Dit nieuwe wachtwoord zal worden gestuurd naar het bij ons bekende e-mail adres.

Wij wensen u succes bij de cursus.

Mochten er nog vragen zijn, kunt u altijd contact opnemen met ons
';

$string['sessionscheduled'] = 'Scheduled for session: ';
$string['sessionsscheduled'] = 'Scheduled for sessions: ';

$string['singledaysession'] = '{$a->userdate_start} from {$a->starttime} until {$a->finishtime}';
$string['multidaysession'] = '{$a->userdate_start} at {$a->starttime} until {$a->userdate_finish} at {$a->finishtime}';
$string['onlinecourse'] = 'Online course';
$string['state_assigned'] = 'Assigned';

$string['caret-right'] = 'Right';
$string['download'] = 'Download';
$string['email'] = 'Email';
$string['enrolment-suspended'] = 'Enrolment suspended';
$string['filter'] = 'Filter';
$string['key'] = 'Key';
$string['plus'] = 'Add';
$string['rating-star'] = 'Rating';
$string['user'] = 'User';
$string['permissions'] = 'Manager';
