<?php
namespace block_manager_dashboard\event;

defined('MOODLE_INTERNAL') || die();

/**
 * The report_log report viewed event class.
 *
 * @property-read array $other {
 *      Extra information about the event.
 *
 *      - int groupid: Group to display.
 *      - int date: Date to display logs from.
 *      - int modid: Module id for which logs were displayed.
 *      - string modaction: Module action.
 *      - string logformat: Log format in which logs were displayed.
 * }
 *
 * @package    report_log
 * @since      Moodle 2.7
 * @copyright  2013 Ankit Agarwal
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class user_assigned extends \core\event\base {

    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'u';
        $this->data['edulevel'] = self::LEVEL_OTHER;
        $this->data['objecttable'] = 'prog';
        $this->context = \context_system::instance();
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('user_assigned', 'block_manager_dashboard');
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        $return = "The user with id '$this->userid' assigned the user '$this->relateduserid' to certification '$this->objectid'. ";
        if(is_array($this->other) && array_key_exists('orderrefrence', $this->other)) $return .= "The provided orderrefrence was '{$this->other['orderrefrence']}'.";
        if(is_array($this->other) && array_key_exists('sessionid', $this->other)) $return .= "The sessionid is '{$this->other['sessionid']}'.";
        return $return;
    }

    /**
     * Return the legacy event log data.
     *
     * @return array
     */
    /*protected function get_legacy_logdata() {
        return array($this->courseid, "course", "report log", "report/log/index.php?id=$this->courseid", $this->courseid);
    }*/

    /**
     * Custom validation.
     *
     * @throws \coding_exception
     * @return void
     */
    protected function validate_data() {
        parent::validate_data();

        if (!isset($this->relateduserid)) {
            throw new \coding_exception('The \'relateduserid\' must be set.');
        }
        
        if (!isset($this->objectid)) {
            throw new \coding_exception('The \'objectid\' value must be set.');
        }

        if (!isset($this->other['orderrefrence'])) {
            throw new \coding_exception('The \'orderrefrence\' value must be set in other.');
        }

        
        if (!isset($this->other['activityname'])) {
            throw new \coding_exception('The \'activityname\' value must be set in other.');
        }

    }

}
