"use strict";

module.exports = function (grunt) {

    // We need to include the core Moodle grunt file too, otherwise we can't run tasks like "amd".
    require("grunt-load-gruntfile")(grunt);
    grunt.loadGruntfile("../../Gruntfile.js");

    
     
    // Load all grunt tasks.
    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-contrib-clean");
 
    var path = require('path'),
        cwd = process.env.PWD || process.cwd(),
        amdSrc = [ cwd + '/amd/src/*.js'];

    /**
     * Function to generate the destination for the uglify task
     * (e.g. build/file.min.js). This function will be passed to
     * the rename property of files array when building dynamically:
     * http://gruntjs.com/configuring-tasks#building-the-files-object-dynamically
     *
     * @param {String} destPath the current destination
     * @param {String} srcPath the  matched src path
     * @return {String} The rewritten destination path.
     */
    var uglifyRename = function(destPath, srcPath) {
        destPath = srcPath.replace('src', 'build');
        destPath = destPath.replace('.js', '.min.js');
        destPath = path.resolve(cwd, destPath);
        return destPath;
    };
 
    grunt.initConfig({
        watch: {
            // If any .less file changes in directory "less" then run the "less" task.
            //files: "less/*.less",
            //tasks: ["less"]
            amd: {
                files: ['amd/src/*.js'],
                tasks: ['amd']
            },
            // Totara: Add less watch target.
            less: {
                files: ['less/*.less'],
                tasks: ['less']
            },
        },
        less: {
            // Production config is also available.
            development: {
                options: {
                    // Specifies directories to scan for @import directives when parsing.
                    // Default value is the directory of the source, which is probably what you want.
                    paths: ["less/"],
                    compress: true
                },
                files: {
                    "styles.css": "less/styles.less"
                }
            },
        },
        eslint: {
            // Even though warnings dont stop the build we don't display warnings by default because
            // at this moment we've got too many core warnings.
            options: {quiet: !grunt.option('show-lint-warnings')},
            amd: {
              src: amdSrc,
              // Check AMD with some slightly stricter rules.
              rules: {
                'no-unused-vars': 'error',
                'no-implicit-globals': 'error'
              }
            },
        },
        uglify: {
            amd: {
                files: [{
                    expand: true,
                    src: amdSrc,
                    rename: uglifyRename
                }],
                options: {report: 'none'}
            },
        },
        
    });
    // The default task (running "grunt" in console).
    grunt.registerTask("default", ["less", "amd"]);
};