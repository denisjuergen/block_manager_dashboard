<?php
$definitions = array(
    'employees' => array(
        'mode' => cache_store::MODE_SESSION
    ),
    'certifications' => array(
        'mode' => cache_store::MODE_APPLICATION
    )
);